
# GT GAME APIS INTEGRATION

## 1. Mô tả dịch vụ

GT Game APIs là hệ thống trung gian cung cấp, quản lý tất cả các kết nối vào Game Backend để thực hiện các thao tác:
####1. Thêm vật phẩm vào game
####2. Truy xuất thông tin server, nhân vật, giao dịch, vật phẩm ...
####3. Và nhiều chức năng khác

## 2. Các hệ thống

**Game Backend APIs** [:link:](/GTAPIs/gamebackend/)

Những APIs cần cung cấp cho GT Game APIs từ Game Backend(do TechOM hoặc Đối tác quản lý) khi các dịch vụ của VNG có nhu cầu sử dụng dữ liệu

**Game Service APIs** [:link:](/GTAPIs/gameservice/)

Những APIs do GT Game APIs cung cấp cho các dịch vụ nội bộ của VNG (như Web Shop, Billing, Promotion, CS, DnD ...)

**User Wallet** [:link:](/GTAPIs/userwallet/)

Chức năng lưu lại tiền thừa khi thanh toán và mua vật phẩm trên hệ thống VNG

## 3. FAQs
