# User Wallet Snapshot

<!-- [English](/docs_gameapis/userwallet_snapshot_en) -->

## 1. Khái niệm

- User Wallet là database lưu trữ tiền thừa của User. Tiền thừa được phát sinh khi User mua vật phẩm với số tiền **cao hơn** hoặc **thấp hơn** so với mệnh giá của vật phẩm. 

- User wallet snapshot là trạng thái dữ liệu của User Wallet được export (trích xuất) tại 1 thời điểm.

- User wallet snapshot được export ra hàng ngày nhằm mục đích backup và làm dữ liệu hỗ trợ đối soát

- User balance là tổng số tiền thừa tồn lại của User tại một thời điểm 

## 2. Dữ liệu

- User Wallet sử dụng [Couchbase](https://www.couchbase.com/) để lưu trữ dữ liệu

- User Wallet Snapshot được export bằng command [cbbackup](https://docs.couchbase.com/server/6.0/cli/cbbackup-tool.html)

- Định dạng file Snapshot là **.cbb**, có thể dùng [SQLite](https://sqlite.org) để truy xuất dữ liệu.  

- Dữ liệu được lưu trữ theo key, value

    - Format:

    | Tên | Kiểu dữ liệu | Ý nghĩa | Ví dụ |
    | --  | -- | -- | -- |
    | key | String | đại diện cho 1 user | TEST-1234567890 | 
    | value | String | dữ liệu thông tin balance của User | ```65|0|TEST-1234567890|4|0|0||O:8:"stdClass":4:{s:7:"balance";d:5200;s:12:"totalBalance";d:27800;s:6:"txncur";i:2;s:3:"txn";a:5:{i:0;s:19:"1326177105943207936";i:1;s:19:"1326177105943207936";i:2;s:19:"1414448619636924416";i:3;s:19:"1322485855633285120";i:4;s:19:"1322485855633285120";}}```|
    
    - key: được tạo từ **$gameID + "-" + $userID**

    - value: là một object

    | **key** | **value** |
    | -- | -- |
    | balance | số tồn **Xu** / **Nguyên Tệ** của user ở thời điểm snapshot |    
    | totalBalance | tổng số **Xu** / **Nguyên Tệ** mà user đã nạp vào User Wallet tính đến thời điểm snapshot |

- Đơn vị của số tồn lưu trong User Wallet được cấu hình theo loại game:

    - Game ở thị trường Việt Nam đơn vị là **Xu** với giá trị tương ứng là **100 VNĐ = 1 XU**
    - Game ở thị trường SEA/Global đơn vị là đồng **Nguyên Tệ** (VD: THB)

## 3. Thao tác xử lý dữ liệu

**Lấy thời gian snapshot**

- Command:

    > \# sqlite3 /tmp/data-0000.cbb 'select val from cbb_meta where key = "start.datetime"'

    ```2020/02/17-00:02:05```

    !!! Note 
        - `start.datetime` là thời gian bắt đầu snapshot (gọi là t0), tác vụ snapshot diễn ra trong 1 khoảng thời gian (gọi là t1)
        - Khoảng thời gian t1 có thể thay đổi ở mỗi lần snapshot
        - Dữ liệu được xem như chốt tại thời điểm t0 + t1. 
    
**Lấy balance của 1 User**

- Command:

    > \# sqlite3 /tmp/data-0000.cbb 'select key,val from cbb_msg where key like "TEST-1234567890"'

    ```65|0|TEST-1234567890|4|0|0||O:8:"stdClass":4:{s:7:"balance";d:5200;s:12:"totalBalance";d:27800;s:6:"txncur";i:2;s:3:"txn";a:5:{i:0;s:19:"1326177105943207936";i:1;s:19:"1326177105943207936";i:2;s:19:"1414448619636924416";i:3;s:19:"1322485855633285120";i:4;s:19:"1322485855633285120";}}```

**Lấy tổng balance của Wallet**

- Command: 

    > \# sqlite3 /tmp/data-0000.cbb 'select * from cbb_msg' | awk -F'"balance";d:' '{print $2}' | awk -F';s:12:"totalBalance"' '{print $1}' | awk '{sum += $1} END {print sum}'
    
    ```100000```

    !!! Note
        Tùy vào game mà tổng balance là **Xu** hay **Nguyên Tệ**

## 4. Lưu ý 

!!! Danger
    User Wallet có nhiều phiên bản và sử dụng theo đặc thù của Game, vì vậy cần được Team vận hành tư vấn trước khi sử dụng tài liệu này.



