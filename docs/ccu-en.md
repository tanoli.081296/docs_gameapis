# GAME BACKEND APIs

## 1. Introdution

This document specifies the APIs that a game shall provides for VNG System to integrate with it


## 2. APIs

**List APIs**

Name|Description
----|-----------
[API getCCU](#api-getccu) |This API is to get CCU of all servers at current time.

<div class="pagebreak"></div>
**APIs Description**

###* **API getCCU** 
* Url: https://$game_api_domain/getCCU
* Protocol: Http GET/POST
* Content-Type: application/x-www-form-urlencoded charset=utf-8
* Input params:

    Parameter Name|Data Type|Description
    :-------------|:--------|:----------
    ts|Long|Current timestamp in milliseconds
    sig|String|Referring to signature generation in [Appendix 1](#appendix-1-create-signature)

* Output params (application/json):

    Parameter Name|Data Type|Description
    --------------|---------|-----------
    returnCode|Integer|Error code <br/>`success` = 1 <br/> `fail` != 1
    returnMessage|String|Error description
    data|Map <String, CcuEntity\>|Server list, key is serverID

* CcuEntity:

    Parameter Name|Data Type|Description
    --------------|---------|-----------
    serverID|String|ID of server
    serverName|String|Name of server
    ccu|Integer|CCU of server
    status|Integer|Status of server <br/> `enabled`=1 <br/> `disabled` !=1<br/>
    addInfo|String|Json object, including all extended information

* Example:

    -|-
    ---|---
    API|getCCU|
    Method|GET
    Input|ts=1562233802731&sig=*
    Output|{"returnCode":1,"returnMessage":"success","data":{"1001":{"serverID":"1001","serverName":"Server 1","ccu":10,"status":1,"addInfo":{"abc":"1"}},"1002":{"serverID":"1002","serverName":"Server 2","ccu":12,"status":1,"addInfo":{"abc":"1"}}}}


<div class="pagebreak"></div>

##**Appendix 1. Create signature**


* Signature is a MD5 hash string of a sequence of parameters with a Secret Key.

* Secret Key is a server-side shared key. This key is assigned to a partner system by VNG system.

* All parameters except the signature itself of an exchange message will participate to form the signature.

* All parameter values that form the signature must sort alphabetically based on parameter name.

* All parameters that form the signature must in their original format (i.e., not a URL encoded).

* All parameters that form the signature are case sensitive. 

* All leading and trailing whitespace of a string will have to be trimmed.   

* Example:

    - URL: <span style="font-size:13px;">"https://$domain/$uri?ts=1517900546676&sig=defcf9d945c15b524f4b647d68ae1b3d";</span>
    - Secret key: <span style="font-size:13px;">"pmn4rcocifttkdla8yrwnp0ntnwwze7rpc"</span>
    - Raw: <span style="font-size:13px;">"pmn4rcocifttkdla8yrwnp0ntnwwze7rpc" + "1517900546676"</span>
    - Sig: <span style="font-size:13px;">md5("pmn4rcocifttkdla8yrwnp0ntnwwze7rpc1517900546676") = "defcf9d945c15b524f4b647d68ae1b3d"</span>