# GAME BACKEND APIs

## 1. Introdution

This document specifies the APIs that a game shall provides for VNG System to integrate with it


## 2. APIs

<!--
_|_|
-------|-|
Version| 1.0
Release date| 2-2-2020
Description| Là danh sách những APIs cần cung cấp cho GT Game APIs từ Game Backend(do TechOM hoặc Đối tác quản lý) khi các dịch vụ của VNG có nhu cầu sử dụng dữ liệu

##Change log
Version|Description|
-|-|
-->


**List APIs**

Name|Description
----|-----------
[API addItems](#api-additems)|This API is for delivering a item to game user


**APIs Description**

<div class="pagebreak"></div>

<div class="pagebreak"></div>

<div class="pagebreak"></div>

<div class="pagebreak"></div>

<div class="pagebreak"></div>

<div class="pagebreak"></div>

<div class="pagebreak"></div>

<div class="pagebreak"></div>


<div class="pagebreak"></div>
<!--addItems-->

###* **API addItems**
* Url: https://$game_api_domain/addItems
* Protocol: Http GET/POST
* Content-Type: application/x-www-form-urlencoded charset=utf-8
* Input params:

    Parameter Name|Data Type|Description
    --------------|---------|-----------    
	userID `required`|String|ID of user    
    serverID `required`|String|ID of server    
    roleID `required`|String|ID of role
	itemID `required`|String|ID of item
	amount|String|amount of item
	pTransID `required`|String|transaction id, unique
    roleName|String|Name of role
	level|String|Level of role	
	addInfo|String|extra info for game
	clientIP|String|IP of user browser
    loginChannel|String|Login channel. <br/> Referring to Login channel definition in [Appendix 2](#appendix-2-login-channel)
    ts `required`|Long|Current timestamp in milliseconds
    sig `required`|String|Referring to signature generation in [Appendix 1](#appendix-1-create-signature)

* Output params (application/json):

    Parameter Name|Data Type|Description
    --------------|---------|-----------
    returnCode `required`|Integer|Error code <br/>`success` = 1<br/> `fail` != 1
    returnMessage|String|Error description    

* Example input/output:
<table>
    <tr>
        <td>API</td>
        <td>addItems</td>
    </tr>
    <tr>
        <td>Method</td>
        <td>POST</td>
    </tr>
    <tr>
        <td>Input</td>
        <td>roleID=1000010&serverID=2&userID=1525197004312092672&itemID=2
        <br>&amount=10&pTransID=100000000000001&ts=1581649596976&sig=***</td>
    </tr>
    <tr>
        <td>Output</td>
        <td>
        ```json
        {
            "returnCode": 1,
            "returnMessage": "SUCCESS"
        }
        ```
        </td>
    </tr>
</table>

<!--endaddItems-->
<div class="pagebreak"></div>

<div class="pagebreak"></div>

<div class="pagebreak"></div>

<div class="pagebreak"></div>

<div class="pagebreak"></div>

##**Appendix 1. Create signature**


* Signature is a MD5 hash string of a sequence of parameters with a Secret Key.

* Secret Key is a server-side shared key. This key is assigned to a partner system by VNG system.

* All parameters except the signature itself of an exchange message will participate to form the signature.

* All parameter values that form the signature must sort alphabetically based on parameter name.

* All parameters that form the signature must in their original format (i.e., not a URL encoded).

* All parameters that form the signature are case sensitive. 

* All leading and trailing whitespace of a string will have to be trimmed.   

* Example:

    - URL: <span style="font-size:13px;">"https://$domain/$uri?userID=12345&roleID=6789&roleName=test&serverID=1 &gameID=game&itemID=item1&sig=0114778e1349ecffed6c920af210c057 &distSrc=pc&timestamp=1517900546676";</span>
    - Secret key: <span style="font-size:13px;">"pmn4rcocifttkdla8yrwnp0ntnwwze7rpc"</span>
    - Raw: <span style="font-size:13px;">"pmn4rcocifttkdla8yrwnp0ntnwwze7rpc" + "pc" + "game" + "item1" + "6789" + "test" + "1" + "1517900546676" + "12345"</span>
    - Sig: <span style="font-size:13px;">md5("pmn4rcocifttkdla8yrwnp0ntnwwze7rpcpcgameitem16789test1151790054667612345") = "0114778e1349ecffed6c920af210c057"</span>


##**Appendix 2. Login channel**
 

Channel ID| Channel Name
----------|-------------
10|Guest/Email
11|Zing ID (uin)
12|Zalo
13|Google
14|Facebook
15|Zing Me (guid)
16|Line
17|Apple ID

