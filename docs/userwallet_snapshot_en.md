# User Wallet Snapshot

<!-- [Tiếng Việt](/docs_gameapis/userwallet_snapshot) -->

## 1.  Some of definitions

- User wallet is a database, which used to store change money of user/gamer’s purchasing transaction. The change money occurred when a gamer purchases an in-game item by a paying amount that is **greater** or **less** than price of the item 

- User wallet snapshot is status of the database that is taken/exported at a time point

- User wallet snapshot is taken daily in purposes of backing up and supporting data cross-checking

- User balance is total of change money of a user account at a point of time.

## 2. User wallet data format

- User Wallet technically has implemented using [Couchbase](https://www.couchbase.com/) for data storage

- User Wallet Snapshot exported by the [cbbackup](https://docs.couchbase.com/server/6.0/cli/cbbackup-tool.html) command

- Snapshot file is **.cbb** type and we can use the [SQLite](https://sqlite.org) to extract data from it  

- A user wallet data item is kind of key-value

    - Format:

    | Name | Data type | Semantic | Example |
    | --  | -- | -- | -- |
    | key | String | identifying 1 user | TEST-1234567890 | 
    | value | String | balance info of the user | ```65|0|TEST-1234567890|4|0|0||O:8:"stdClass":4:{s:7:"balance";d:5200;s:12:"totalBalance";d:27800;s:6:"txncur";i:2;s:3:"txn";a:5:{i:0;s:19:"1326177105943207936";i:1;s:19:"1326177105943207936";i:2;s:19:"1414448619636924416";i:3;s:19:"1322485855633285120";i:4;s:19:"1322485855633285120";}}```|
    
    - key: composed from **$gameID + "-" + $userID**

    - value: is an object

    | **key** | **value** |
    | -- | -- |
    | balance | the user balance at snapshot time point |    
    | totalBalance | total of **Xu** / **Original currency** that user has paid and debited into the wallet till snapshot time |

- The unit of balance value of user wallet is configured according to the region/market in which a game operates:

    - In Vietnam market, the unit called **Xu** with fix rate of 100VND (i.e., **100 VNĐ = 1 XU**)
    - In SEA/Global market, the unit is equal to **Original currency** (i.e., THB for the Thailand market, IDR for the Indonesia…etc.).

## 3. User wallet snapshotting

**The snapshot time point**

- Using command:

    > \# sqlite3 /tmp/data-0000.cbb 'select val from cbb_meta where key = "start.datetime"'

    ```2020/02/17-00:02:05```

    !!! Note 
        - The `start.datetime` is time point at which the snapshot process starts (as t0 ) and then will finish at t1
        - The snapshot period (is t1-t0) is possible varied at each time of performing snapshot. The average of the latency is 45 seconds and varying by the data amount of the wallet (or number of user purchasing transactions)
        - The snapshot data is considered settling after t1. 
    
**Query User Balance**

- Using command:

    > \# sqlite3 /tmp/data-0000.cbb 'select key,val from cbb_msg where key like "TEST-1234567890"'

    ```65|0|TEST-1234567890|4|0|0||O:8:"stdClass":4:{s:7:"balance";d:5200;s:12:"totalBalance";d:27800;s:6:"txncur";i:2;s:3:"txn";a:5:{i:0;s:19:"1326177105943207936";i:1;s:19:"1326177105943207936";i:2;s:19:"1414448619636924416";i:3;s:19:"1322485855633285120";i:4;s:19:"1322485855633285120";}}```

**Wallet balance calculating**

- Using command: 

    > \# sqlite3 /tmp/data-0000.cbb 'select * from cbb_msg' | awk -F'"balance";d:' '{print $2}' | awk -F';s:12:"totalBalance"' '{print $1}' | awk '{sum += $1} END {print sum}'
    
    ```100000```

    !!! Note
        The output is balance value only and the amount is inferred by the unit (market of the game, **Xu** or name of **original currency**) 

## 4. Warranty 

!!! Danger
    The User Wallet has deployed in various versions corresponding to each specific game’s requirement. So, please consult operation team for its correctness before one uses this document to send out.



