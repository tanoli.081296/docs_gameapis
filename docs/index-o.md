#GT Game APIs Doc

##APIs for Other Team Integration

_|_|
-------|-|
Version| 1.1
Release date| 23-08-2019
Description| This document specifies the APIs that a game shall provides for VNG System to integrate with it

##Change log
Version|Description|
-|-|

##List APIs

_|Name|Description
-|----|-----------
1|[API setCodes](#1-api-setcodes) |This API is to set code for gamer
2|[API addItems](#2-api-additems)|This API is to add items for gamer
3|[API getCCU](#3-api-getccu) |This API is to list CCU status of all servers of game
4|[API getTopRanking](#4-api-gettopranking)|This API is to list top role by ranking value of all servers
5|[API getServers](#5-api-getservers) |This API is to list all servers of game (in case game has more than one server)
6|[API getServersByUserID](#6-api-getservers-by-userid) |This API is to list all servers which user has role (get by UserID). 
7|[API getRoles](#7-api-getroles)|This API is to get role/account’s information by UserID && ServerID
8|[API getRolesByRoleID](#8-api-getroles-by-roleid)|This API is to get role/account’s information by RoleID
9|[API getRolesByUserID](#9-api-getroles-by-userid)|This API is to get role/account’s information by UserID

<div class="pagebreak"></div>

###1. API setCodes 
* Url: https://$game_api_domain/setCodes
* Protocol: Http GET/POST
* Content-Type: application/x-www-form-urlencoded charset=utf-8
* Input params:

    Parameter Name|Data Type|Description
    --------------|---------|-----------    
	userID|String (required) |ID of user    
    serverID|String (required) |ID of server    
    roleID|String (required) |ID of role    
	code|String (required) |code of role
	pTransID|String (required) |transaction id, unique
	roleName|String|Name of role
	level|String|Level of role	
	addInfo|String|extra info for game
	clientIP|String|IP of user browser
    loginChannel|String|Login channel. <br/> Referring to Login channel definition in [Appendix 2](#appendix-2-login-channel)
    ts|Long (required) |Current timestamp in milliseconds
    sig|String (required) |Referring to signature generation in [Appendix 1](#appendix-1-create-signature)

* Output params (application/json):

    Parameter Name|Data Type|Description
    --------------|---------|-----------
    returnCode|Integer|Error code <br/>`success` = 1<br/> `fail` != 1
    returnMessage|String|Error description    


<div class="pagebreak"></div>

###2. API addItems
* Url: https://$game_api_domain/addItems
* Protocol: Http GET/POST
* Content-Type: application/x-www-form-urlencoded charset=utf-8
* Input params:

    Parameter Name|Data Type|Description
    --------------|---------|-----------    
	userID|String (required) |ID of user    
    serverID|String (required) |ID of server    
    roleID|String (required) |ID of role
	itemID|String (required) |ID of item
	amount|String (required) |amount of item
	pTransID|String (required) |transaction id, unique
    roleName|String|Name of role
	level|String|Level of role	
	addInfo|String|extra info for game
	clientIP|String|IP of user browser
    loginChannel|String|Login channel. <br/> Referring to Login channel definition in [Appendix 2](#appendix-2-login-channel)
    ts|Long (required) |Current timestamp in milliseconds
    sig|String (required) |Referring to signature generation in [Appendix 1](#appendix-1-create-signature)

* Output params (application/json):

    Parameter Name|Data Type|Description
    --------------|---------|-----------
    returnCode|Integer|Error code <br/>`success` = 1<br/> `fail` != 1
    returnMessage|String|Error description    


<div class="pagebreak"></div>

###3. API getCCU
* Url: https://$game_api_domain/getCCU
* Protocol: Http GET/POST
* Content-Type: application/x-www-form-urlencoded charset=utf-8
* Input params:

    Parameter Name|Data Type|Description
    :-------------|:--------|:----------
    clientIP|String|IP of user browser
    ts|Long|Current timestamp in milliseconds
    sig|String|Referring to signature generation in [Appendix 1](#appendix-1-create-signature)

* Output params (application/json):

    Parameter Name|Data Type|Description
    --------------|---------|-----------
    returnCode|Integer|Error code <br/>`success` = 1 <br/> `fail` != 1
    returnMessage|String|Error description
    data|List<CcuEntity\>|List CCU of server

* CcuEntity:

    Parameter Name|Data Type|Description
    --------------|---------|-----------
    serverID|String|ID of server
    serverName|String|Name of server
    status|Integer|Status of server <br/> `enabled`=1 <br/> `disabled` !=1<br/>
	ccu|Integer|CUU of server <br/>
	
<div class="pagebreak"></div>

###4. API getTopRanking
* Url: https://$game_api_domain/getTopRanking
* Protocol: Http GET/POST
* Content-Type: application/x-www-form-urlencoded charset=utf-8
* Input params:

    Parameter Name|Data Type|Description
    :-------------|:--------|:----------
    clientIP|String|IP of user browser
	rankTyle|String|Ranking type. Example: power, level ...	
    ts|Long|Current timestamp in milliseconds
    sig|String|Referring to signature generation in [Appendix 1](#appendix-1-create-signature)

* Output params (application/json):

    Parameter Name|Data Type|Description
    --------------|---------|-----------
    returnCode|Integer|Error code <br/>`success` = 1 <br/> `fail` != 1
    returnMessage|String|Error description
    data|Map <String, RankingEntity\>|Role list

* RankingEntity:

    Parameter Name|Data Type|Description
    --------------|---------|-----------
    roleID|String|ID of role
    roleName|String|Name of role
    userID|String|ID of user
    serverID|String|ID of server which role playing
    loginChannel|String|Login channel. <br/> Referring to Login channel definition in [Appendix 2](#appendix-2-login-channel)
    rankValue|String|Ranking value of role
    info|Object (json)|More detail information of role	
	
<div class="pagebreak"></div>

###5. API getServers 
* Url: https://$game_api_domain/getServers
* Protocol: Http GET/POST
* Content-Type: application/x-www-form-urlencoded charset=utf-8
* Input params:

    Parameter Name|Data Type|Description
    :-------------|:--------|:----------
    clientIP|String|IP of user browser
    ts|Long|Current timestamp in milliseconds
    sig|String|Referring to signature generation in [Appendix 1](#appendix-1-create-signature)

* Output params (application/json):

    Parameter Name|Data Type|Description
    --------------|---------|-----------
    returnCode|Integer|Error code <br/>`success` = 1 <br/> `fail` != 1
    returnMessage|String|Error description
    data|Map <String, ServerEntity\>|Server list, key is serverID

* ServerEntity:

    Parameter Name|Data Type|Description
    --------------|---------|-----------
    serverID|String|ID of server
    serverName|String|Name of server
    status|Integer|Status of server <br/> `enabled`=1 <br/> `disabled` !=1<br/>If status is not enabled, it'll not display on Web Shop 

<div class="pagebreak"></div>

###6. API getServers by UserID 
* Url: https://$game_api_domain/getServersByUserID
* Protocol: Http GET/POST
* Content-Type: application/x-www-form-urlencoded charset=utf-8
* Input params:

    Parameter Name|Data Type|Description
    :-------------|:--------|:----------
	userID|String|ID of user	
    clientIP|String|IP of user browser
    ts|Long|Current timestamp in milliseconds
    sig|String|Referring to signature generation in [Appendix 1](#appendix-1-create-signature)

* Output params (application/json):

    Parameter Name|Data Type|Description
    --------------|---------|-----------
    returnCode|Integer|Error code <br/>`success` = 1 <br/> `fail` != 1
    returnMessage|String|Error description
    data|Map <String, ServerEntity\>|Server list, key is serverID

* ServerEntity:

    Parameter Name|Data Type|Description
    --------------|---------|-----------
    serverID|String|ID of server
    serverName|String|Name of server
    status|Integer|Status of server <br/> `enabled`=1 <br/> `disabled` !=1<br/>If status is not enabled, it'll not display on Web Shop 


<div class="pagebreak"></div>

###7. API getRoles
 
* Url: https://$game_api_domain/getRoles
* Protocol: Http GET/POST
* Content-Type: application/x-www-form-urlencoded charset=utf-8
* Input params:

    Parameter Name|Data Type|Description
    --------------|---------|-----------
    clientIP|String|IP of user browser
    userID|String|ID of user
    loginChannel|String|Login channel. <br/> Referring to Login channel definition in [Appendix 2](#appendix-2-login-channel)
    serverID|String|ID of server
    roleID|String|ID of role
    roleName|String|Name of role
    ts|Long|Current timestamp in milliseconds
    sig|String|Referring to signature generation in [Appendix 1](#appendix-1-create-signature)

* Output params (application/json):

    Parameter Name|Data Type|Description
    --------------|---------|-----------
    returnCode|Integer|Error code <br/>`success` = 1 <br/> `fail` != 1
    returnMessage|String|Error description
    data|Map <String, RoleEntity\>|Role list, key is roleID

* RoleEntity: 

    Parameter Name|Data Type|Description
    --------------|---------|-----------
    roleID|String|ID of role
    roleName|String|Name of role
	userID|String|ID of user
    serverID|String|ID of server which role playing    
    serverName|String|Name of server which role playing
    info|Object (json)|More detail information of role which want to display on Web Shop <br/> Example: level, rank, gold ...

<div class="pagebreak"></div>

###8. API getRoles by RoleID
 
* Url: https://$game_api_domain/getRolesByRoleID
* Protocol: Http GET/POST
* Content-Type: application/x-www-form-urlencoded charset=utf-8
* Input params:

    Parameter Name|Data Type|Description
    --------------|---------|-----------
    clientIP|String|IP of user browser    
    roleID|String|ID of role    
    ts|Long|Current timestamp in milliseconds
    sig|String|Referring to signature generation in [Appendix 1](#appendix-1-create-signature)

* Output params (application/json):

    Parameter Name|Data Type|Description
    --------------|---------|-----------
    returnCode|Integer|Error code <br/>`success` = 1 <br/> `fail` != 1
    returnMessage|String|Error description
    data|Map <String, RoleEntity\>|Role list, key is roleID

* RoleEntity: 

    Parameter Name|Data Type|Description
    --------------|---------|-----------
    roleID|String|ID of role
    roleName|String|Name of role
    userID|String|ID of user
    serverID|String|ID of server which role playing
    serverName|String|Name of server which role playing
    info|Object (json)|More detail information of role which want to display on Web Shop <br/> Example: level, rank, gold ...

<div class="pagebreak"></div>

###9. API getRoles by UserID
 
* Url: https://$game_api_domain/getRolesByUserID
* Protocol: Http GET/POST
* Content-Type: application/x-www-form-urlencoded charset=utf-8
* Input params:

    Parameter Name|Data Type|Description
    --------------|---------|-----------
    clientIP|String|IP of user browser    
    userID|String|ID of user    
    ts|Long|Current timestamp in milliseconds
    sig|String|Referring to signature generation in [Appendix 1](#appendix-1-create-signature)

* Output params (application/json):

    Parameter Name|Data Type|Description
    --------------|---------|-----------
    returnCode|Integer|Error code <br/>`success` = 1 <br/> `fail` != 1
    returnMessage|String|Error description
    data|Map <String, RoleEntity\>|Role list, key is roleID

* RoleEntity: 

    Parameter Name|Data Type|Description
    --------------|---------|-----------
    roleID|String|ID of role
    roleName|String|Name of role
    userID|String|ID of user
    serverID|String|ID of server which role playing
    serverName|String|Name of server which role playing
    info|Object (json)|More detail information of role which want to display on Web Shop <br/> Example: level, rank, gold ...

<div class="pagebreak"></div>

##Appendix 1. Create signature

* Signature is a MD5 hash string of a sequence of parameters with a Secret Key.

* Secret Key is a server-side shared key. This key is assigned to a partner system by VNG system.

* All parameters except the signature itself of an exchange message will participate to form the signature.

* All parameter values that form the signature must sort alphabetically based on parameter name.

* All parameters that form the signature must in their original format (i.e., not a URL encoded).

* All parameters that form the signature are case sensitive. 

* All leading and trailing whitespace of a string will have to be trimmed.  

* Example:

    ```
    URL: "https://$domain/$uri?userID=12345&roleID=6789&roleName=test&serverID=1&gameID=game&itemID=item1&sig=6d1ecb0d9e9d118e032f9c8b5f0ad866&distSrc=pc&timestamp=1517900546676";
    Secret key: "pmn4rcocifttkdla8yrwnp0ntnwwze7rpc"
    Raw: "pmn4rcocifttkdla8yrwnp0ntnwwze7rpc" + "pc" + "game" + "item1" + "6789" + "test" + "1" + "1517900546676" + "12345"
    Sig: md5("pmn4rcocifttkdla8yrwnp0ntnwwze7rpcpcgameitem16789test1151790054667612345") = "0114778e1349ecffed6c920af210c057"
    ```

##Appendix 2. Login channel
 
Channel ID| Channel Name
----------|-------------
10|Guest/Email
11|Zing ID (uin)
12|Zalo
13|Google
14|Facebook
15|Zing Me (guid)
16|Line
17|Apple 
 
##Appendix 3. Fulfillment guaranty 

1. Base on VNG’s [Conversion Policy](../docs_policy_process/policy.md#ge-payment-conversion-policy-sep-2018) and denomination list of payment channels, the amount for fulfillment decision (i.e., delivery the item or not) may be different with the cost of buying item.
 
2. A Game server must base on (1) the Conversion Rate of the Game and (2) the paying amount (i.e., the amount parameter in CallbackBilling API) to decide on delivery a buying item and/or other in-game resources to the user or not.

* Example:  Given a monthly itemA having cost is 100,000 VND, meaning user must pay full 100,000 VND to get it. Cases of the amount parameter in [API callbackBilling](#5-api-callbackbilling) will be:
    
    * Case 1: amount = 100,000 
        
        -> Game server delivers the itemA as a normal case
    
    * Case 2: amount = 120,000 
    
        -> Game server delivers the itemA and 200 in-game Diamond using the 100VND/1 Diamond game’s Conversion rate
    
    * Case 3: amount = 70,000 
    
        -> Game server decides not deliver itemA and only gives the user 700 in-game Diamond using 100VND/1 diamond game’s conversion rate