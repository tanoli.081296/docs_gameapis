# User-wallet

## 1. Nhu cầu phát sinh user-wallet 
* Thị trường VN và SEA tồn tại các phương thức thanh toán giới hạn theo các mệnh giá(denomination paying amount) như: thẻ cào(prepaid card), SMS, DCB… và thậm chí một số ví điện tử như TrueWallet, LinePay… Các phương tiện thanh toán này có độ phủ cao ở phân khúc under-bank users và được sử dụng cho kênh local payment.
* Mặc dù giá bán của các items/package có thể được thiết kế sao cho phù hợp theo bảng mệnh giá(biết trước) của các phương thức thanh toán sẽ triển khai, nhưng vẫn không loại trừ hết khả năng phát sinh tiền thanh toán dư thừa(change). 
    * Ví dụ: Đối với thẻ cào(prepaid card), giá trị thanh toán hay mệnh giá thẻ được sử dụng sẽ chỉ xác định được sau khi user đã thực hiện thanh toán. Tiền thừa(change) sẽ nảy sinh nếu giá trị thanh toán(mệnh giá thẻ đã write-off) không đủ để mua item, hoặc giá trị thanh toán lớn hơn giá bán của item đặt mua.
* Như vậy, để đáp ứng local payment và đảm bảo quyền lợi cho user, chức năng lưu lại tiền thừa(change) là bắt buộc phải có - chức năng này đã quen gọi là user-wallet. 
    * Mục đích chính:
        * Giúp giữ lại tiền thừa(change) nếu có phát sinh sau mỗi giao dịch mua vật phẩm in-game.
        * Tích lũy và cho phép user sử dụng để mua(đổi) lấy vật phẩm in-game.
    * Phát sinh và không khuyến khích:
        * Do đặc thù của các phương tiện thanh toán qua thẻ cào, nên user có thể vô tình/cố tình gạch các thẻ mệnh giá lớn và có thể tích lũy số dư trong user-wallet mà không thực hiện tiêu(đổi) lấy vật phẩm.

## 2. Luồng thanh toán & giao hàng
### **- Trường hợp VNG giữ tiền tồn của user:**
* Thực tế triển khai đã phát sinh yêu cầu cài đặt user-wallet như một phần trong nghiệp vụ fulfillment do phía đối tác không thay đổi được hệ thống.
    * Nhận thông báo trạng thái giao dịch thanh toán (thành công) của đơn hàng.
    * (user-wallet) Cập nhật và kiểm tra số dư - Thể hiện bằng Hình 1
    * Gửi thông báo tới Game để yêu cầu giao item cho user đặt mua. 

    &nbsp;&nbsp;
    ![Hình 1: Mô tả luồng thanh toán có lưu tiền tồn của user.](../images/user-wallet-img1.png)    
    <center>***Hình 1: Mô tả luồng thanh toán có lưu tiền tồn của user.***</center>
    &nbsp;&nbsp;

* Lượng tiền thừa (change) được lưu tại user-wallet được thể hiện(cho user nhận biết) bằng đơn vị Point theo điều khoản bổ sung cho từng game triển khai ở VN(lí do pháp chế).
    * Point là đơn vị ảo trong game.
    * Có thời hạn và cách sử dụng theo chính sách của mỗi game.
    * Point không được chuyển đổi từ game này sang game kia.
* Phát sinh luồng thanh toán – cho phép user đổi vật phẩm bằng một số lượng Point tồn(nếu số Point đủ so với giá bán của vật phẩm niêm yết bằng Point) - Thể hiện bằng Hình 2.


    &nbsp;&nbsp;
    ![Hình 2: Mô tả luồng thanh toán trong đó user sử dụng Point tồn để đổi lấy item.](../images/user-wallet-img2.png)
    <center>***Hình 2: Mô tả luồng thanh toán trong đó user sử dụng Point tồn để đổi lấy item.***</center>
    &nbsp;&nbsp;

### **- Trường hợp đối tác nhận toàn bộ giá trị thanh toán (convert-all)**
    
* Toàn bộ số tiền user đã thanh toán, hay giá trị thực thu (net amount) được chuyển đổi (convert) thành các đơn vị trong game (ví dụ như diamond) và sau đó có thể đổi thành các item/package trong game mà user đặt mua, phần tiền thừa (change) sẽ được lưu trữ trong game. Luồng thanh toán này thường gọi tắt là Convert-All, mô tả ở Hình 3.
* Để đáp ứng mô hình này, cần phía đối tác game cung cấp:
    * Đơn vị ảo trong game, ví dụ: diamond, gem…etc
    * Cơ chế quản lý ví (hay túi) để user lưu giữ và chuyển đổi đơn vị ảo thành các vật phẩm, gói tài nguyên.
* Đã có các game đang được triển khai theo mô hình này (CF,KVM,MUSSEA,..). Tuy nhiên, do để thống nhất data log, nên chức năng user-wallet vẫn được cài đặt như Hình 3 và trong đó số dư (balance) luôn có giá trị bằng 0.

    &nbsp;&nbsp;
    ![Hình 3: Mô tả luồng thanh toán mà số dư của user được lưu trữ và quản lý ở phía game (convert-all).](../images/user-wallet-img3.png)
    <center>***Hình 3: Mô tả luồng thanh toán mà số dư của user được lưu trữ và quản lý ở phía game (convert-all).***</center>
    &nbsp;&nbsp;


