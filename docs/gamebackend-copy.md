# GAME BACKEND APIs

## 1. Mô tả dịch vụ

Là những APIs mà Game Server(do TechOM hoặc Đối tác quản lý) cần cung cấp cho GT Game APIs khi các dịch vụ của VNG có nhu cầu sử dụng dữ liệu


## 2. APIs

<!--
_|_|
-------|-|
Version| 1.0
Release date| 2-2-2020
Mô tả| Là danh sách những APIs cần cung cấp cho GT Game APIs từ Game Backend(do TechOM hoặc Đối tác quản lý) khi các dịch vụ của VNG có nhu cầu sử dụng dữ liệu

##Change log
Version|Mô tả|
-|-|
-->

##Hình 1: Web Shop user flow
![Web Shop user flow](../images/wpflow.jpg)

**Danh sách APIs**

Name|Description
----|-----------
[API getServers](#api-getservers) |Liệt kê danh sách tất cả các server của game(trong trường hợp game có nhiều server)
[API addItems](#api-additems)|Trao quà in-game cho người chơi
[API setCodes](#api-setcodes)|Trao code cho người chơi
[API getCCU](#api-getccu) |Lấy thông tin CCU của tất cả server của game
[API getTopRanking](#api-gettopranking)|Lấy top nhân vật theo 1 tiêu chí nào đấy
[URL format](#url-format) |Quy định định dạng của liên kết được tạo ra khi mở trang thanh toán từ trong game


<div class="pagebreak"></div>
**Tài liệu mô tả APIs**

<!--getServers-->
<div class="pagebreak"></div>

###* **API getServers** 
* Url: https://$game_api_domain/getServers
* Protocol: Http GET/POST
* Content-Type: application/x-www-form-urlencoded charset=utf-8
* Input params:

    Tham số|Kiểu dữ liệu|Mô tả
    :-------------|:--------|:----------
    clientIP|String|IP người chơi
    ts|Long|timestamp thời gian hiện tại, đơn vị mili giây
    sig|String|Chữ ký được tạo theo quy tắc [Appendix 1](#appendix-1-create-signature)

* Output params (application/json):

    Tham số|Kiểu dữ liệu|Mô tả
    --------------|---------|-----------
    returnCode|Integer|Mã lỗi <br/>`Thành công` = 1 <br/> `Thất bại` != 1
    returnMessage|String|Mô tả lỗi
    data|Map <String, ServerEntity\>|Danh sách server, key là ID của server

* ServerEntity:

    Tham số|Kiểu dữ liệu|Mô tả
    --------------|---------|-----------
    serverID|String|ID của server
    serverName|String|Tên của server
    status|Integer|Trạng thái của server <br/> `Hiển thị`=1 <br/> `Ẩn` !=1<br/>Chỉ những server được cho phép mới hiển thị trên trang thanh tóan 

* Ví dụ:

    -|-
    ---|---
    API|getServers|
    Phương thức|GET
    Tham số|clientIP=49.213.81.56&ts=1562233802731&sig=*
    Kết quả|{"returnCode":1,"returnMessage":"success","data":{"7001":{"serverID":"7001","serverName":"Server 1","status":1,"info":[]},"7002":{"serverID":"7002","serverName":"Server 2","status":1,"info":[]}}}


<!--endgetServers-->








<!--addItems-->

<div class="pagebreak"></div>

###* **API addItems**
* Url: https://$game_api_domain/addItems
* Protocol: Http GET/POST
* Content-Type: application/x-www-form-urlencoded charset=utf-8
* Input params:

    Parameter Name|Data Type|Description
    --------------|---------|-----------    
	userID|String (required) |ID of user    
    serverID|String (required) |ID of server    
    roleID|String (required) |ID of role
	itemID|String (required) |ID of item
	amount|String (required) |amount of item
	pTransID|String (required) |transaction id, unique
    roleName|String|Name of role
	level|String|Level of role	
	addInfo|String|extra info for game
	clientIP|String|IP of user browser
    loginChannel|String|Login channel. <br/> Referring to Login channel definition in [Appendix 2](#appendix-2-login-channel)
    ts|Long (required) |Current timestamp in milliseconds
    sig|String (required) |Referring to signature generation in [Appendix 1](#appendix-1-create-signature)

* Output params (application/json):

    Parameter Name|Data Type|Description
    --------------|---------|-----------
    returnCode|Integer|Error code <br/>`success` = 1<br/> `fail` != 1
    returnMessage|String|Error description    


<!--endaddItems-->
<!--setCodes-->

<div class="pagebreak"></div>

###* **API setCodes** 
* Url: https://$game_api_domain/setCodes
* Protocol: Http GET/POST
* Content-Type: application/x-www-form-urlencoded charset=utf-8
* Input params:

    Parameter Name|Data Type|Description
    --------------|---------|-----------    
	userID|String (required) |ID of user    
    serverID|String (required) |ID of server    
    roleID|String (required) |ID of role    
	code|String (required) |code of role
	pTransID|String (required) |transaction id, unique
	roleName|String|Name of role
	level|String|Level of role	
	addInfo|String|extra info for game
	clientIP|String|IP of user browser
    loginChannel|String|Login channel. <br/> Referring to Login channel definition in [Appendix 2](#appendix-2-login-channel)
    ts|Long (required) |Current timestamp in milliseconds
    sig|String (required) |Referring to signature generation in [Appendix 1](#appendix-1-create-signature)

* Output params (application/json):

    Parameter Name|Data Type|Description
    --------------|---------|-----------
    returnCode|Integer|Error code <br/>`success` = 1<br/> `fail` != 1
    returnMessage|String|Error description    

<!--endsetCodes-->
<!--getCCU-->

<div class="pagebreak"></div>

###* **API getCCU** 
* Url: https://$game_api_domain/getCCU
* Protocol: Http GET/POST
* Content-Type: application/x-www-form-urlencoded charset=utf-8
* Input params:

    Parameter Name|Data Type|Description
    :-------------|:--------|:----------
    clientIP|String|IP of user browser
    ts|Long|Current timestamp in milliseconds
    sig|String|Referring to signature generation in [Appendix 1](#appendix-1-create-signature)

* Output params (application/json):

    Parameter Name|Data Type|Description
    --------------|---------|-----------
    returnCode|Integer|Error code <br/>`success` = 1 <br/> `fail` != 1
    returnMessage|String|Error description
    data|List<CcuEntity\>|List CCU of server

* CcuEntity:

    Parameter Name|Data Type|Description
    --------------|---------|-----------
    serverID|String|ID of server
    serverName|String|Name of server
    status|Integer|Status of server <br/> `enabled`=1 <br/> `disabled` !=1<br/>
	ccu|Integer|CUU of server <br/>
	
<!--endgetCCU-->
<!--getTopRanking-->

<div class="pagebreak"></div>

###* **API getTopRanking** 
* Url: https://$game_api_domain/getTopRanking
* Protocol: Http GET/POST
* Content-Type: application/x-www-form-urlencoded charset=utf-8
* Input params:

    Parameter Name|Data Type|Description
    :-------------|:--------|:----------
    clientIP|String|IP of user browser
	rankTyle|String|Ranking type. Example: power, level ...	
    ts|Long|Current timestamp in milliseconds
    sig|String|Referring to signature generation in [Appendix 1](#appendix-1-create-signature)

* Output params (application/json):

    Parameter Name|Data Type|Description
    --------------|---------|-----------
    returnCode|Integer|Error code <br/>`success` = 1 <br/> `fail` != 1
    returnMessage|String|Error description
    data|Map <String, RankingEntity\>|Role list

* RankingEntity:

    Parameter Name|Data Type|Description
    --------------|---------|-----------
    roleID|String|ID of role
    roleName|String|Name of role
    userID|String|ID of user
    serverID|String|ID of server which role playing
    loginChannel|String|Login channel. <br/> Referring to Login channel definition in [Appendix 2](#appendix-2-login-channel)
    rankValue|String|Ranking value of role
    info|Object (json)|More detail information of role	
	

<!--endgetTopRanking-->
<!--format-->

<div class="pagebreak"></div>

###* **URL Format**
* Protocol: Http GET
* Content-Type: application/x-www-form-urlencoded charset=utf-8
* Input params:

    Tham số|Kiểu dữ liệu|Mô tả
    :-------------|:--------|:----------
    lang|String|Ngôn ngữ. Theo chuẩn ISO 639-1 codes. Ví dụ: vi,en,… Tham khảo [ISO 639-1 codes](https://en.wikipedia.org/wiki/List_of_ISO_639-1_codes).    
    country|String|Quốc gia. Theo chuẩn ISO 3166-1 alpha-2 codes. Ví dụ: VN,TH,IN,… Tham khảo [ISO 3166-1 alpha-2](https://en.wikipedia.org/wiki/ISO_3166-1_alpha-2#Officially_assigned_code_elements).    
    currency|String|Tiền tệ. Theo chuẩn ISO 4217. Ví dụ: VND,THB,USD,… Tham khảo [ISO 4217](https://en.wikipedia.org/wiki/ISO_4217#Active_codes).
    userID|String|ID của người chơi    
    roleID|String|ID của nhân vật
    roleName|String|Tên nhân vật
    serverID|String|ID của server
    appID|String|
    productID|String|Là vật phẩm mà người chơi chọn mua. Trang thanh toán chỉ hiển thị duy nhất vật phẩm này. (Nếu giá trị này không truyền hoặc truyền với giá trị là 0 thì trang thanh toán sẽ hiển thị tất cả vật phẩm)
    timestamp|String|Timestamp, thời gian hiện tại(13 digits)
    distSrc|String|Source for this transaction (utm_source)    
    sig|String|Chữ ký. Được tạo bằng cách mã hóa MD5 tất cả các thông tin cần gởi cùng với 1 mật mã.(Cách tạo Sig này khác với các API khác)

<!--endformat-->

<div class="pagebreak"></div>

##**Appendix 1. Create signature**


* Chữ ký là 1 chuỗi mã hóa MD5 của các giá trị tham số đã được sắp xếp cùng với một mật mã.

* Mật mã này được tạo bởi VNG hoặc đối tác của VNG và cung cấp cho nhau.

* Tất cả các tham số ngoại trừ Chữ ký sẽ được sử dụng để tạo Chữ ký.

* Các tham số được sắp xếp theo thứ tự ABC.

* Các tham số không được mã hóa.

* Các tham số phân biệt hoa - thường

* Các khoảng trắng đầu và cuối chuỗi sẽ bị loại bỏ.  

* Ví dụ:

    - URL: <span style="font-size:13px;">"https://$domain/$uri?userID=12345&roleID=6789&roleName=test&serverID=1 &gameID=game&itemID=item1&sig=0114778e1349ecffed6c920af210c057 &distSrc=pc&timestamp=1517900546676";</span>
    - Secret key: <span style="font-size:13px;">"pmn4rcocifttkdla8yrwnp0ntnwwze7rpc"</span>
    - Raw: <span style="font-size:13px;">"pmn4rcocifttkdla8yrwnp0ntnwwze7rpc" + "pc" + "game" + "item1" + "6789" + "test" + "1" + "1517900546676" + "12345"</span>
    - Sig: <span style="font-size:13px;">md5("pmn4rcocifttkdla8yrwnp0ntnwwze7rpcpcgameitem16789test1151790054667612345") = "0114778e1349ecffed6c920af210c057"</span>


##**Appendix 2. Login channel**
 

Channel ID| Channel Name
----------|-------------
10|Guest/Email
11|Zing ID (uin)
12|Zalo
13|Google
14|Facebook
15|Zing Me (guid)
16|Line
17|Apple ID

 
##**Appendix 3. Fulfillment guaranty** 



1. Dựa trên chính sách chuyển đổi của VNG và danh sách chuyển đổi của các kênh thanh toán thì số  tiền dùng để mua vật phẩm(tham số amount của API CallbackBilling) sẽ có thể khác số tiền mà người chơi đã thanh toán.
 
2. Dựa trên tỉ giá chuyển đổi cùng với số  tiền nhận được(tham số amount của API CallbackBilling) thì phía Game server quyết định có trao vật phẩm mà người chơi đã chọn mua và các tài nguyên trong game khác hay không.

 **Ví dụ: Vật phẩm mua theo tháng A có giá 100,000 VND, nghĩa là người chơi phải trả 100,000 VND để mua vật phẩm này. Tùy theo giá trị của tham số  *amount* trong [API callbackBilling](#api-callbackbilling) như sau:**
    
 -  Trường hợp 1: amount = 100,000 
        
    - Game Server giao vật phẩm A bình thường cho người chơi
    
 -  Trường hợp 2: amount = 120,000 
    
    - Game Server giao 1 vật phẩm A vào giao thêm 200 Kim cương in-game(dựa trên tỉ giá 100VND/1 kim cương) cho người chơi
    
 -  Trường hợp 3: amount = 70,000 
    
    - Game Server không giao vật phẩm A và chỉ giao 700 Kim cương in-game(dựa trên tỉ giá 100VND/1 kim cương) cho người chơi


##**Appendix 4. Duplication Guaranty** 


1. Tất cả giao dịch giữa hệ thống VNG và Game Server phải được xác định bằng `tnxid` và `tnxid` là duy nhất cho một giao dịch
2. Hệ thống Game Server phải có khả năng kiểm tra nếu một giao dịch được thực hiện nhiều lần(chống trao nhầm nhiều lần vật phẩm cho 1 giao dịch). 
3. Nếu có các giao dịch không thành công thì hệ thống VNG sẽ tự động gọi giao hàng lại tới hệ Game Server.  
4. Do 2 hệ thống VNG và Game Server là 2 hệ thống độc lập và bất đồng bộ nên có thể sảy ra trường hợp người chơi có thể mua nhiều hơn 1 lần với vật phẩm đặc biệt(luật chỉ cho phép mua 1 lần duy nhất). Vì vậy phía Game Server nên có cơ chế đền bù cho người chơi trong trường hợp này.

**Ví dụ: Gói thẻ tháng cho phép người chơi chỉ mua 1 lần duy nhất trong tháng.**

 -  Lần 1 người chơi mua gói thẻ tháng bằng hình thức thanh toán ngân hàng thì thanh toán bị chậm từ phía ngân hàng và người chơi chưa nhận được vật phẩm
 -  Lần 2 người chơi tiếp tục mua lại gói thẻ tháng bằng Zalopay và nhận hàng thành công
 -  Sau đó phía ngân hàng thông báo thanh toán thành công cho lần 1. Hệ thống VNG thực hiện giao hàng và quá trình giao hàng bị thất bại vì vi phạm quy định "Gói thẻ tháng người chơi chỉ mua 1 lần duy nhất trong tháng"
 -  Vì vậy phía Game Server và phía vận hành nên có cơ chế đền bù cho người chơi trong trường hợp này.
 -  Đề xuất các phương án đền bù:
    -  Phía Game Server tự động trao người chơi 1 số tiền in-game tương ứng
    -  Phía Game Server tự động trao người chơi 1 vật phẩm tương ứng
    -  Phía vận hành game đền bù thủ công cho người chơi thông qua GM-Tool