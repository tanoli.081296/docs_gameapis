# GAME SERVICE INTEGRATION

## 1. Mô tả dịch vụ

APIs do GT Game APIs cung cấp cho các dịch vụ nội bộ của VNG (như Web Shop, Billing, Promotion, CS, DnD ...)

## 2. APIs

<!--
_|_|
-------|-|
Version| 1.0
Release date| 2-2-2020
Mô tả| APIs do GT Game APIs cung cấp cho các dịch vụ nội bộ của VNG (như Web Shop, Billing, Promotion, CS, DnD ...)

##Change log
Phiên bản|Mô tả|
-|-|

-->
**Danh sách APIs**

<table>
    <tr>
        <th style="min-width:10%"></th>
        <th width="25%">Tên</th>
        <th>Mô tả</th>
    </tr>
    <tr>
        <td>1</td>
        <td><a href="#1-api-getservers">API getServers</a></td>
        <td>Liệt kê danh sách tất cả các server của game(trong trường hợp game có nhiều server)</td>
    </tr>
    <tr>
        <td>2</td>
        <td><a href="#2-api-getservers-by-userid">API getServersByUserID</a></td>
        <td>Liệt kê danh sách tất cả các server của game mà người chơi có tạo nhân vật(trong trường hợp game có nhiều server)</td>
    </tr>
    <tr>
        <td>3</td>
        <td><a href="#3-api-getroles">API getRoles</a></td>
        <td>Lấy thông tin của tài khoản/nhân vật theo UserID && ServerID</td>
    </tr>
    <tr>
        <td>4</td>
        <td><a href="#4-api-getroles-by-roleid">API getRolesByRoleID</a></td>
        <td>Lấy thông tin của tài khoản/nhân vật theo  RoleID</td>
    </tr>
    <tr>
        <td>5</td>
        <td><a href="#5-api-getroles-by-userid">API getRolesByUserID</a></td>
        <td>Lấy thông tin của tài khoản/nhân vật theo  UserID</td>
    </tr>
    <tr>
        <td>6</td>
        <td><a href="#6-api-getproducts">API getProducts</a></td>
        <td>Lấy thông tin vật phẩm theo 1 số điều kiện nhất định(trong trường hợp game có 1 số quy định khi mua vật phẩm)</td>
    </tr>
    <tr>
        <td>7</td>
        <td><a href="#7-api-gettrans">API getTrans</a></td>
        <td>Lấy thông transaction ID khi người chơi chọn 1 vật phẩm để mua. Khi người chơi đã thanh toán mua vật phẩm thành công thì transaction ID được gởi về  game cùng với thông tin vật phẩm đã mua nhằm mục đích kiểm chứng</td>
    </tr>    
    <tr>
        <td>8</td>
        <td><a href="#8-api-callbackbilling">API callbackBilling</a></td>
        <td>Thực hiện trao phẩm đã mua cho người chơi</td>
    </tr>
    <tr>
        <td>9</td>
        <td><a href="#9-api-setcodes">API setCodes</a></td>
        <td>Nhập code đổi quà cho người chơi</td>
    </tr>
    <tr>
        <td>10</td>
        <td><a href="#10-api-additems">API addItems</a></td>
        <td>Trao quà cho người chơi</td>
    </tr>
    <tr>
        <td>11</td>
        <td><a href="#11-api-getccu">API getCCU</a></td>
        <td>Lấy thông tin CCU của tất cả các server trong game</td>
    </tr>
    <tr>
        <td>12</td>
        <td><a href="#12-api-gettopranking">API getTopRanking</a></td>
        <td>Lấy thông tin bảng xếp hạng của game</td>
    </tr>
    <tr>
        <td>13</td>
        <td><a href="#13-api-getplayerinfo">API getPlayerInfo</a></td>
        <td>Lấy thông tin chi tiết của nhân vật/ game, sử dụng cho các event</td>
    </tr>
</table>

**Các môi trường tích hợp APIs**

|Môi trường|Domain|Mô tả
--|---------|----------------------------
development|https://api-dev.mto.zing.vn| Xử lý dữ liệu ở môi trường Test của game
sandbox|https://api-sandbox.mto.zing.vn| Xử lý dữ liệu ở môi trường Test của game
staging|https://api-stg.mto.zing.vn| Xử lý dữ liệu ở môi trường Live của game
production|https://api.mto.zing.vn| Xử lý dữ liệu ở môi trường Live của game

**Tài liệu mô tả APIs**

###1. API getServers 
* Url: https://$gt_api_domain/app/api/get/getServers
* Protocol: Http GET/POST
* Content-Type: application/x-www-form-urlencoded charset=utf-8
* Input params:

    Tham số|Kiểu dữ liệu|Mô tả
    :-------------|:--------|:----------
    clientIP|String|IP browser của người chơi
    serverMode|String|loại server trả về<br/>`Trả về tất cả server` = "getAll" <br/> `chỉ trả server đang hoạt động` != "getAll"
    appID `required`|String|appID cung cấp bởi GT-Framework
    apiKey `required`|String|apiKey cung cấp bởi GT-Framework
    ts `required`|Long|Timestamp hiện tại, đơn vị mili giây
    sig `required`|String|Chữ ký được tạo theo quy tắc [Appendix 1](#appendix-1-create-signature)

* Output params (application/json):

    Tham số|Kiểu dữ liệu|Mô tả
    --------------|---------|-----------
    returnCode|Integer|Mã lỗi <br/>`thành công` = 1 <br/> `thất bại` != 1
    returnMessage|String|Mô tả lỗi
    data|Map <String, ServerEntity\>|Danh sách server, key là ID server

* ServerEntity:

    Tham số|Kiểu dữ liệu|Mô tả
    --------------|---------|--------------
    serverID|String|ID của server
    serverName|String|Tên của server
    status|Integer|Trạng thái của server <br/> `Hiển thị`=1 <br/> `Ẩn` !=1<br/>Chỉ những server cho phép mới được hiển thị trên trang thanh toán 
    info `optional`|Object (json)|Thông tin thêm của server <br/> Ví dụ: ID gộp server ...

* Example input/output:
<table>
    <tr>
        <td>API</td>
        <td>getServers</td>
    </tr>
    <tr>
        <td>Method</td>
        <td>POST</td>
    </tr>
    <tr>
        <td>Input</td>
        <td>
        apiKey=APIKEY123456789&appID=tj<br>&serverMode=getAll&ts=1581590104021&userID=&sig=***
        </td>
    </tr>
    <tr>
        <td>Output</td>
        <td>
        ```json
        {
            "returnCode": 1,
            "returnMessage": "SUCCESS",
            "data": {
                "110": {
                    "serverName": "s110.Thần Sơn",
                    "serverID": "110",
                    "status": 0,
                    "info": {
                        "merge": "1",
                        "name": "s110.Thần Sơn",
                        "active": false,
                        "id": "110"
                    }
                }
            }
        }
        ```
        </td>
    </tr>
</table>

###2. API getServers by UserID 
* Url: https://$gt_api_domain/app/api/get/getServersByUserID
* Protocol: Http GET/POST
* Content-Type: application/x-www-form-urlencoded charset=utf-8
* Input params:

    Tham số|Kiểu dữ liệu|Mô tả
    :-------------|:--------|:----------
    clientIP|String|IP browser của người chơi
    userID `required`|String|ID của người chơi
    serverMode|String|loại server trả về<br/>`Trả về tất cả server` = "getAll" <br/> `Chỉ trả server đang hoạt động` != "getAll"
    appID `required`|String|appID cung cấp bởi GT-Framework
    apiKey `required`|String|apiKey cung cấp bởi GT-Framework
    ts `required`|Long|Timestamp hiện tại, đơn vị mili giây
    sig `required`|String|Chữ ký được tạo theo quy tắc [Appendix 1](#appendix-1-create-signature)

* Output params (application/json):

    Tham số|Kiểu dữ liệu|Mô tả
    --------------|---------|-----------
    returnCode|Integer|Mã lỗi <br/>`thành công` = 1 <br/> `thất bại` != 1
    returnMessage|String|Mô tả lỗi
    data|Map <String, ServerEntity\>|Danh sách server, key là ID server

* ServerEntity:

    Tham số|Kiểu dữ liệu|Mô tả
    --------------|---------|-----------
    serverID|String|ID của server
    serverName|String|Tên của server
    status|Integer|Trạng thái của server <br/> `Hiển thị`=1 <br/> `Ẩn` !=1<br/>Chỉ những server cho phép mới được hiển thị trên trang thanh toán
    info `optional`|Object (json)|Thông tin thêm của server <br/> Ví dụ: ID gộp server ...

* Example input/output:
<table>
    <tr>
        <td>API</td>
        <td>getServersByUserID</td>
    </tr>
    <tr>
        <td>Method</td>
        <td>POST</td>
    </tr>
    <tr>
        <td>Input</td>
        <td>apiKey=APIKEY123456789&appID=cdmt&loginType=11<br>&ts=1581590104840&userID=1525197004312092672&sig=***</td>
    </tr>
    <tr>
        <td>Output</td>
        <td>
        ```json
        {
            "returnCode": 1,
            "returnMessage": "SUCCESS",
            "data": {
                "22": {
                    "serverName": "S20 Từ Ninh Cung",
                    "serverID": "22",
                    "status": 1,
                    "info": {
                        "serverName": "S20 Từ Ninh Cung",
                        "serverID": "22",
                        "status": 1,
                        "info": ""
                    }
                }
            }
        }
        ```
        </td>
    </tr>
</table>
    

###3. API getRoles
* Url: https://$gt_api_domain/app/api/get/getRoles
* Protocol: Http GET/POST
* Content-Type: application/x-www-form-urlencoded charset=utf-8
* Input params:

    Tham số|Kiểu dữ liệu|Mô tả
    --------------|---------|-----------
    clientIP|String|IP browser của người chơi
    userID `required`|String|ID của người chơi
    loginType `required`|String|Kênh đăng nhập. <br/> Tham chiếu kênh đăng nhập [Appendix 2](#appendix-2-login-type)
    serverID `required`|String|ID của server
    roleID|String|ID của nhân vật
    roleName|String|Tên nhân vật
    appID `required`|String|appID cung cấp bởi GT-Framework
    apiKey `required`|String|apiKey cung cấp bởi GT-Framework
    ts `required`|Long|Timestamp hiện tại, đơn vị mili giây
    sig `required`|String|Chữ ký được tạo theo quy tắc [Appendix 1](#appendix-1-create-signature)

* Output params (application/json):

    Tham số|Kiểu dữ liệu|Mô tả
    --------------|---------|-----------
    returnCode|Integer|Mã lỗi <br/>`thành công` = 1 <br/> `thất bại` != 1
    returnMessage|String|Mô tả lỗi
    data|Map <String, RoleEntity\>|Danh sách nhân vật, key là ID nhân vật

* RoleEntity: 

    Tham số|Kiểu dữ liệu|Mô tả
    --------------|---------|-----------
    roleID|String|ID của nhân vật
    roleName|String|Tên nhân vật
    level `optional`|String|Level nhân vật
    userID|String|ID của người chơi
    serverID|String|ID của server của nhân vật này    
    serverName|String|Tên của server của nhân vật này    
    info `optional`|Object (json)|Những thông tin khác của nhân vật. Ví dụ xếp hạng, lực chiến ...
    
* Example input/output:
<table>
    <tr>
        <td>API</td>
        <td>getRoles</td>
    </tr>
    <tr>
        <td>Method</td>
        <td>POST</td>
    </tr>
    <tr>
        <td>Input</td>
        <td>apiKey=APIKEY123456789&appID=cdmt&loginType=11<br>&roleID=&roleName=&serverID=2&ts=1581590104892<br>&userID=1525197004312092672&sig=***</td>
    </tr>
    <tr>
        <td>Output</td>
        <td>
        ```json
        {
            "returnCode": "1",
            "returnMessage": "success",
            "data": {
                "7011100": {
                    "roleID": 7011100,
                    "roleName": "-Ke-Huy-Diet-",
                    "serverID": "2",
                    "serverName": "",
                    "info": ""
                }
            }
        }
        ```
        </td>
    </tr>
</table>
    
###4. API getRoles by RoleID
* Url: https://$gt_api_domain/app/api/get/getRolesByRoleID
* Protocol: Http GET/POST
* Content-Type: application/x-www-form-urlencoded charset=utf-8
* Input params:

    Tham số|Kiểu dữ liệu|Mô tả
    --------------|---------|-----------
    clientIP|String|IP browser của người chơi
    loginType|String|Kênh đăng nhập. <br/> Tham chiếu kênh đăng nhập [Appendix 2](#appendix-2-login-type)
    roleID `required`|String|ID của nhân vật
    appID `required`|String|appID cung cấp bởi GT-Framework
    apiKey `required`|String|apiKey cung cấp bởi GT-Framework
    ts `required`|Long|Timestamp hiện tại, đơn vị mili giây
    sig `required`|String|Chữ ký được tạo theo quy tắc [Appendix 1](#appendix-1-create-signature)
    
* Output params (application/json):

    Tham số|Kiểu dữ liệu|Mô tả
    --------------|---------|-----------
    returnCode|Integer|Mã lỗi <br/>`thành công` = 1 <br/> `thất bại` != 1
    returnMessage|String|Mô tả lỗi
    data|Map <String, RoleEntity\>|Danh sách nhân vật, key là ID nhân vật

* RoleEntity: 

    Tham số|Kiểu dữ liệu|Mô tả
    --------------|---------|-----------
    roleID|String|ID của nhân vật
    roleName|String|Tên nhân vật
    level `optional`|String|Level nhân vật
    userID|String|ID của người chơi
    serverID|String|ID của server của nhân vật này    
    serverName|String|Tên của server của nhân vật này    
    info `optional`|Object (json)|Những thông tin khác của nhân vật. Ví dụ level, xếp hạng, lực chiến ...
    
* Example input/output:
<table>
    <tr>
        <td>API</td>
        <td>getRolesByRoleID</td>
    </tr>
    <tr>
        <td>Method</td>
        <td>POST</td>
    </tr>
    <tr>
        <td>Input</td>
        <td>apiKey=APIKEY123456789&appID=autochess&loginType=6<br>&roleID=YC3I5&ts=1581590104928&sig=***</td>
    </tr>
    <tr>
        <td>Output</td>
        <td>
        ```json
        {
            "returnCode": 1,
            "returnMessage": "SUCCESS",
            "data": {
                "YC3I5": {
                    "loginType": "11",
                    "level": "",
                    "roleID": "YC3I5",
                    "roleName": "VoDanh",
                    "serverName": "Server 1",
                    "userID": "YC3I5",
                    "serverID": "1",
                    "info": {
                        "roleID": "YC3I5",
                        "roleName": "VoDanh"
                    }
                }
            }
        }
        ```
        </td>
    </tr>
</table>

###5. API getRoles by UserID
* Url: https://$gt_api_domain/app/api/get/getRolesByUserID
* Protocol: Http GET/POST
* Content-Type: application/x-www-form-urlencoded charset=utf-8
* Input params:

    Tham số|Kiểu dữ liệu|Mô tả
    --------|--|----------------
    clientIP|String|IP browser của người chơi
    userID `required`|String|ID của người chơi
    loginType `required`|String|Kênh đăng nhập. <br/> Tham chiếu kênh đăng nhập [Appendix 2](#appendix-2-login-type)
    appID `required`|String|appID cung cấp bởi GT-Framework
    apiKey `required`|String|apiKey cung cấp bởi GT-Framework
    ts `required`|Long|Timestamp hiện tại, đơn vị mili giây
    sig `required`|String|Chữ ký được tạo theo quy tắc [Appendix 1](#appendix-1-create-signature)    

* Output params (application/json):

    Tham số|Kiểu dữ liệu|Mô tả
    --------------|---------|-----------
    returnCode|Integer|Mã lỗi <br/>`thành công` = 1 <br/> `thất bại` != 1
    returnMessage|String|Mô tả lỗi
    data|Map <String, RoleEntity\>|Danh sách nhân vật, key là ID nhân vật

* RoleEntity: 

    Tham số|Kiểu dữ liệu|Mô tả
    --------------|---------|-----------
    roleID|String|ID của nhân vật
    roleName|String|Tên nhân vật
    level `optional`|String|Level nhân vật
    userID|String|ID của người chơi
    serverID|String|ID của server của nhân vật này    
    serverName|String|Tên của server của nhân vật này    
    info `optional`|Object (json)|Những thông tin khác của nhân vật. Ví dụ level, xếp hạng, lực chiến ...
    
* Example input/output:
<table>
    <tr>
        <td>API</td>
        <td>getRolesByUserID</td>
    </tr>
    <tr>
        <td>Method</td>
        <td>POST</td>
    </tr>
    <tr>
        <td>Input</td>
        <td>apiKey=APIKEY123456789&appID=hpt&loginType=11<br>&ts=1581592822433&userID=1200695056299851776&sig=***</td>
    </tr>
    <tr>
        <td>Output</td>
        <td>
        ```json
        {
            "returnCode": 1,
            "returnMessage": "SUCCESS",
            "data": {
                "158630165": {
                    "loginType": "11",
                    "level": "",
                    "roleID": "158630165",
                    "roleName": "Phác SơẢnh",
                    "serverName": "S165-Tiêu Liên Ý",
                    "userID": "1200695056299851776",
                    "serverID": "180010165",
                    "info": {
                        "sn": "S165-Tiêu Liên Ý",
                        "rn": "Phác SơẢnh",
                        "rid": "158630165",
                        "sid": "180010165"
                    }
                }
            }
        }
        ```
        </td>
    </tr>
</table>


###6. API getProducts 
* Url: https://$gt_api_domain/app/api/get/getProducts
* Protocol: Http GET/POST
* Content-Type: application/x-www-form-urlencoded charset=utf-8
* Input params:

    Tham số|Kiểu dữ liệu|Mô tả
    --------------|---------|-----------
    clientIP|String|IP browser của người chơi
    userID `required`|String|ID của người chơi
    loginType|String|Kênh đăng nhập. <br/> Tham chiếu kênh đăng nhập [Appendix 2](#appendix-2-login-type)
    serverID `required`|String|ID của server
    serverName|String|Tên của server
    roleID `required`|String|ID của nhân vật
    roleName|String|Tên nhân vật
	country `required`|String|ISO 3166-1 alpha-2 code. E.g VN,TH,IN,…Tham khảo [ISO 3166-1 alpha-2](https://en.wikipedia.org/wiki/ISO_3166-1_alpha-2#Officially_assigned_code_elements).
	currency `required`|String|ISO 4217. E.g VND,THB,USD,… Tham khảo [ISO 4217](https://en.wikipedia.org/wiki/ISO_4217#Active_codes).
    appID `required`|String|appID cung cấp bởi GT-Framework
    apiKey `required`|String|apiKey cung cấp bởi GT-Framework
    ts `required`|Long|Timestamp hiện tại, đơn vị mili giây
    sig `required`|String|Chữ ký được tạo theo quy tắc [Appendix 1](#appendix-1-create-signature)

* Output params (application/json):

    Tham số|Kiểu dữ liệu|Mô tả
    --------------|---------|-----------
    returnCode|Integer|Error code <br/>`success` = 1<br/> `fail` != 1
    returnMessage|String|Mô tả lỗi
    data|Map <String, ProductEntity\>|Danh sách vật phẩm, key là ID vật phẩm

* ProductEntity: 

    Tham số|Kiểu dữ liệu|Mô tả
    --------------|---------|-----------
    productID|String|ID của vật phẩm
    productName|String|Tên vật phẩm
    enable|Boolean|Trạng thái của vật phẩm <br/> `Hiển thị`=1 <br/> `Ẩn` !=1<br/>
    productDescription `optional`|String|Mô tả thông tin vật phẩm
    productBonus `optional`|String|Thông tin khác
    info `optional`|Object(json)|Những thông tin khác cần hiển thị trên trang thanh toán  

* Example input/output:
<table>
    <tr>
        <td>API</td>
        <td>getProducts</td>
    </tr>
    <tr>
        <td>Method</td>
        <td>POST</td>
    </tr>
    <tr>
        <td>Input</td>
        <td>apiKey=APIKEY123456789&appID=cdmt&clientIP=49.213.81.56<br>&country=VN&currency=VND&loginType=11&roleID=1000010&roleName=&serverID=2<br>&serverName=&ts=1581648024947&userID=1525197004312092672&sig=***</td>
    </tr>
    <tr>
        <td>Output</td>
        <td>
        ```json
        {
            "returnCode": 1,
            "returnMessage": "SUCCESS",
            "data": {
                "8000vang.cungdau.gs2.vng": {
                    "productValue": 9223372036854776000,
                    "productID": "8000vang.cungdau.gs2.vng",
                    "productName": "Gói 8000 Vàng",
                    "enabled": true,
                    "info": ""
                }
            }
        }
        ```
        </td>
    </tr>
</table>

###7. API getTrans
* Url: https://$gt_api_domain/app/api/get/getTrans
* Protocol: Http GET/POST
* Content-Type: application/x-www-form-urlencoded charset=utf-8
* Input params:

    Tham số|Kiểu dữ liệu|Mô tả
    --------------|---------|-----------
    clientIP|String|IP browser của người chơi
    userID `required`|String|ID của người chơi
    loginType `required`|String|Kênh đăng nhập. <br/> Tham chiếu kênh đăng nhập [Appendix 2](#appendix-2-login-type)
    serverID `required`|String|ID của server
    roleID `required`|String|ID của nhân vật
    roleName|String|Tên nhân vật
    productID `required`|String|ID của vật phẩm hoặc gói vật phẩm mà người chơi mua
    amount `required`|String|Giá vật phẩm mà người chơi mua
    clientID|String|ID của trang thanh toán(trong trường hợp game có nhiều trang thanh toán)
    orderNumber `required`|Long|Transaction ID được tạo bởi hệ thống VNG, duy nhất
    country `required`|String|ISO 3166-1 alpha-2 code. E.g VN,TH,IN,…Tham khảo [ISO 3166-1 alpha-2](https://en.wikipedia.org/wiki/ISO_3166-1_alpha-2#Officially_assigned_code_elements).
    currency `required`|String|ISO 4217. E.g VND,THB,USD,… Tham khảo [ISO 4217](https://en.wikipedia.org/wiki/ISO_4217#Active_codes).
    appID `required`|String|appID cung cấp bởi GT-Framework
    apiKey `required`|String|apiKey cung cấp bởi GT-Framework
    ts `required`|Long|Timestamp hiện tại, đơn vị mili giây
    sig `required`|String|Chữ ký được tạo theo quy tắc [Appendix 1](#appendix-1-create-signature)

* Output params (application/json):

    Tham số|Kiểu dữ liệu|Mô tả
    --------------|---------|-----------
    returnCode|Integer|Error code <br/>`success` = 1;<br/>`fail` != 1
    returnMessage|String|Mô tả lỗi
    data|AppTransEntity Object|Thông tin mã giao dịch

* AppTransEntity:

    Tham số|Kiểu dữ liệu|Mô tả
    --------------|---------|-----------
    appTransID|String|Transaction ID của Game
    appAddInfo `optional`|String|Định dạng Json object. Thường chứa thông tin `addinfo` dùng cho [API Billing Callback](#8-api-callbackbilling) và các thông tin mở rộng khác
    orderID|String|Transaction ID của VNG
    info `optional`|Object(json)|Thông tin thêm nhận được từ server game

* Example input/output:
<table>
    <tr>
        <td>API</td>
        <td>getTrans</td>
    </tr>
    <tr>
        <td>Method</td>
        <td>POST</td>
    </tr>
    <tr>
        <td>Input</td>
        <td>amount=20000&apiKey=APIKEY123456789&appID=cdmt<br>&clientID=&clientIP=49.213.81.56&country=VN&currency=VND&loginType=11<br>&orderNumber=100000000000001&productID=80vang.cungdau.gs2.vng<br>&roleID=21008237&roleName=&serverID=21&serverName=<br>&ts=1581648691257&userID=1545386448968974336&sig=***</td>
    </tr>
    <tr>
        <td>Output</td>
        <td>
        ```json
        {
            "returnCode": 1,
            "returnMessage": "SUCCESS",
            "data": {
                "appTransID": "21-21008237-5777-1581648703",
                "orderID": "100000000000001",
                "appAddInfo": "1",
                "info": {
                    "returnCode": 1,
                    "data": {
                        "appTransID": "21-21008237-5777-1581648703",
                        "appAddInfo": "1"
                    },
                    "returnMessage": "success"
                }
            }
        }
        ```
        </td>
    </tr>
</table>

###8. API callbackBilling
* Url: https://$gt_api_domain/app/api/add/callbackBilling
* Protocol: Http GET/POST
* Content-Type: application/x-www-form-urlencoded charset=utf-8
* Input params:

    Tham số|Kiểu dữ liệu|Mô tả
    ------------|---|-----------
    txnid `required`|String|Transaction ID được tạo bởi hệ thống VNG, duy nhất (chính là orderNumber của api getTrans)
    apptxnid `required`|String|Transaction ID, là kết quả trả về `appTransID` khi gọi api [API getTrans](#7-api-gettrans). Đây là thông tin dùng để chứng thực giao dịch.
    userid `required`|String|ID của người chơi sẽ nhận vật phẩm trong game
    gameid `required`|String|ID của game (assigned by VNG)
    serverid `required`|String|ID server của người chơi đang chơi
    items `required`|String|ID của vật phẩm hoặc gói vật phẩm mà người chơi mua
    amount `required`|String|Thông tin này dùng để quyết định vật phẩm có được giao cho người chơi hay không<br/>Tham khảo [Appendix 3](#appendix-3-fulfillment-guaranty)    
    amount_currency|String|Đơn vị tiền tệ tương ứng của `amount`    
    amount_origin|String|Là amount của vật phẩm nhận được từ from Google/Apple Store, sử dụng trong trường hợp thanh toán IAP (chấp nhận giá trị rỗng hoặc null)
    currency_origin|String|Đơn vị tiền tệ tương ứng của `amount_origin`. `currency_origin` = null nếu `amount_origin` = null
    amount_country|String|Là amount của vật phẩm nhận được từ from Google/Apple Store, sử dụng trong trường hợp thanh toán IAP (chấp nhận giá trị rỗng hoặc null)
    currency_country|String|Đơn vị tiền tệ tương ứng của `amount_country`. `currency_country` = null nếu `amount_country` = null
    unum `required`|String|ID của nhân vật trong game, là kết quả trả về `roleID` khi gọi api [API getRoles](#3-api-getroles)	
    
    Tham số|Kiểu dữ liệu|Mô tả(cont)
    ------------|---|-----------
    channel `required`|String|Kênh thanh toán: zalopay, zing card ...	
    paymethod `required`|String|Phương thức thanh toán: Webpay là <strong>"web"</strong>, android là <strong>"sdk"</strong>, ios là <strong>"sdk"</strong>, huawei là <strong>"sdk"</strong>
    platform `required`|String|Webpay là <strong>"web"</strong>, android là <strong>"android"</strong>, ios là <strong>"ios"</strong>, huawei là <strong>"huawei"</strong>
    addinfo|String|Những thông tin thêm khi cần dùng, là kết quả trả về  `appAddInfo` khi gọi api [API getTrans](#7-api-gettrans) (có thể truyền rỗng hoặc null) 
    appID `required`|String|appID cung cấp bởi GT-Framework
    apiKey `required`|String|apiKey cung cấp bởi GT-Framework
    ts `required`|Long|Timestamp hiện tại, đơn vị mili giây
    sig `required`|String|Chữ ký được tạo theo quy tắc [Appendix 1](#appendix-1-create-signature)

* Output params (application/json):

    Tham số|Kiểu dữ liệu|Mô tả
    --------------|---------|-----------
    returnCode|Integer|Mã lỗi <br/>`thành công` = 1;<br/> `trùng giao dịch` = 2 <br/> `thất bại` < 1
    returnMessage|String|Mô tả lỗi
    data `optional`|Object(json)|Thông tin thêm nhận được từ server game

* Example input/output:
<table>
    <tr>
        <td>API</td>
        <td>callbackBilling</td>
    </tr>
    <tr>
        <td>Method</td>
        <td>POST</td>
    </tr>
    <tr>
        <td>Input</td>
        <td>addinfo=Mw%3D%3D&amount=20000&amount_country=&amount_origin=
        <br>&apiKey=APIKEY123456789&appID=cdmt
        <br>&apptxnid=1-1000010-1000394-1574234809&channel=1&currency_country=
        <br>&currency_origin=&gameid=cdmt&items=&paymethod=web&platform=web&serverid=2
        <br>&ts=1581649146984&txnid=100000000000001&unum=1000010
        <br>&userid=1525197004312092672&sig=***</td>
    </tr>
    <tr>
        <td>Output</td>
        <td>
        ```json
        {
            "returnCode": 1,
            "returnMessage": "SUCCESS",
            "data": {
                "returnCode": 1,
                "returnMessage": "success"
            }
        }
        ```
        </td>
    </tr>
</table>

###9. API setCodes 
* Url: https://$gt_api_domain/app/api/add/setCodes
* Protocol: Http GET/POST
* Content-Type: application/x-www-form-urlencoded charset=utf-8
* Input params:

    Tham số|Kiểu dữ liệu|Mô tả
    --------------|---------|-----------    
	userID `required`|String|ID của người chơi
    serverID `required`|String|ID của server
    roleID `required`|String|ID của nhân vật
	code `required`|String|Mã code cần nhập
	pTransID `required`|String|Transaction ID, là duy nhất
	addInfo|String|Thông tin thêm cần truyền cho Game
	clientIP|String|IP browser của người chơi
    appID `required`|String|appID cung cấp bởi GT-Framework
    apiKey `required`|String|apiKey cung cấp bởi GT-Framework
    ts `required`|Long|Timestamp hiện tại, đơn vị mili giây
    sig `required`|String|Chữ ký được tạo theo quy tắc [Appendix 1](#appendix-1-create-signature)

* Output params (application/json):

    Tham số|Kiểu dữ liệu|Mô tả
    --------------|---------|-----------
    returnCode|Integer|Error code <br/>`success` = 1<br/> `fail` != 1
    returnMessage|String|Mô tả lỗi
    data `optional`|Object(json)|Thông tin thêm nhận được từ server game

* returnCode:

    returnCode|returnMessage|Mô tả
    --|-----|----------------
    1|SUCCESS|Nhập code thành công
    -6|SET_CODES_ERROR|Lỗi không xác định
    -50|ACCOUNT_NOT_ONLINE_OR_NOT_EXIST|Tài khoản/nhân vật không online hoặc không tồn tại
    -44|CODE_NOT_FOUND|Code không tồn tại
    -45|CODE_TIME_EXPIRED|code chưa active/code hết hạn
    -46|CODE_HAS_BEEN_USED|code đã sử dụng
    -47|CODE_ACTIVE_TYPE_ERROR|đã nhập code cùng loại
    -41|DUPLICATED_TRANS_ERROR|pTransID bị trùng

* Example input/output:
<table>
    <tr>
        <td>API</td>
        <td>setCodes</td>
    </tr>
    <tr>
        <td>Method</td>
        <td>POST</td>
    </tr>
    <tr>
        <td>Input</td>
        <td>addInfo=&apiKey=APIKEY123456789&appID=cdmt&code=603add
        <br>&pTransID=100000000000001&roleID=1000010&clientIP=1.1.1.1
        <br>&serverID=2&ts=1581649492178&userID=1525197004312092672&sig=***</td>
    </tr>
    <tr>
        <td>Output</td>
        <td>
        ```json
        {
            "returnCode": 1,
            "returnMessage": "SUCCESS",
            "data": {
                "errCode": 1,
                "errMessage": "success"
            }
        }
        ```
        </td>
    </tr>
</table>

###10. API addItems
* Url: https://$gt_api_domain/app/api/add/addItems
* Protocol: Http GET/POST
* Content-Type: application/x-www-form-urlencoded charset=utf-8
* Input params:

    Tham số|Kiểu dữ liệu|Mô tả
    --------------|---------|-----------
    userID `required`|String|ID của người chơi
    serverID `required`|String|ID của server
    roleID `required`|String|ID của nhân vật
    itemID `required`|String|ID của item cần gửi cho user
	amount `required`|String|Số lượng item muốn add
    pTransID `required`|String|Transaction ID, là duy nhất
    addInfo|String|Thông tin thêm cần truyền cho Game
	clientIP|String|IP browser của người chơi
    appID `required`|String|appID cung cấp bởi GT-Framework
    apiKey `required`|String|apiKey cung cấp bởi GT-Framework
    ts `required`|Long|Timestamp hiện tại, đơn vị mili giây
    sig `required`|String|Chữ ký được tạo theo quy tắc [Appendix 1](#appendix-1-create-signature)

* Output params (application/json):

    Tham số|Kiểu dữ liệu|Mô tả
    --------------|---------|-----------
    returnCode|Integer|Mã lỗi <br/>`thành công` = 1;<br/> `trùng pTransID` = -41 <br/> `thất bại` < 1
    returnMessage|String|Mô tả lỗi
    data `optional`|Object(json)|Thông tin thêm nhận được từ server game

* Example input/output:
<table>
    <tr>
        <td>API</td>
        <td>addItems</td>
    </tr>
    <tr>
        <td>Method</td>
        <td>POST</td>
    </tr>
    <tr>
        <td>Input</td>
        <td>addInfo=&apiKey=APIKEY123456789&appID=cdmt&itemID=item1&amount=11
        <br>pTransID=100000000000001&roleID=1000010&serverID=2&clientIP=1.1.1.1
        <br>&ts=1581649596976&userID=1525197004312092672&sig=***</td>
    </tr>
    <tr>
        <td>Output</td>
        <td>
        ```json
        {
            "returnCode": 1,
            "returnMessage": "SUCCESS",
            "data": {
                "resultCode": 1,
                "resultMessage": "success",
                "data": {
                    "2": 10
                }
            }
        }
        ```
        </td>
    </tr>
</table>

###11. API getCCU
* Url: https://$gt_api_domain/app/api/get/getCCU
* Protocol: Http GET/POST
* Content-Type: application/x-www-form-urlencoded charset=utf-8
* Input params:

    Tham số|Kiểu dữ liệu|Mô tả
    :-------------|:--------|:----------
    clientIP|String|IP browser của người chơi
    appID `required`|String|appID cung cấp bởi GT-Framework
    apiKey `required`|String|apiKey cung cấp bởi GT-Framework
    ts `required`|Long|Timestamp hiện tại, đơn vị mili giây
    sig `required`|String|Chữ ký được tạo theo quy tắc [Appendix 1](#appendix-1-create-signature)

* Output params (application/json):

    Tham số|Kiểu dữ liệu|Mô tả
    --------------|---------|-----------
    returnCode|Integer|Mã lỗi <br/>`thành công` = 1 <br/> `thất bại` != 1
    returnMessage|String|Mô tả lỗi
    data|List<CcuEntity\>|Danh sách server và CCU tương ứng

* CcuEntity:

    Tham số|Kiểu dữ liệu|Mô tả
    --------------|---------|-----------
    unixtime|String|thời gian đẩy log ccu 
    group_id|String|ID của group chứa server (nếu product ko có group thì để "") 
    group_name|String|Tên của group chứa server (nếu product ko có group thì để "")
    server_list|List<CcuServerEntity\>|Mảng chứa danh sách ccu trên các server

* CcuServerEntity:

    Tham số|Kiểu dữ liệu|Mô tả
    --------------|---------|-----------
    server_id|String|ID của server  
    server_name `optional`|String|Name của server  
    status `optional`|Integer|Trạng thái của server<br/> `Hoạt động`= 1 <br/> `Không hoạt động`= 1
    ccu|Integer|Số lượng gamer đang chơi game trên 1 server

* Example input/output:
<table>
    <tr>
        <td>API</td>
        <td>getCCU</td>
    </tr>
    <tr>
        <td>Method</td>
        <td>POST</td>
    </tr>
    <tr>
        <td>Input</td>
        <td>apiKey=APIKEY123456789&appID=tj
        <br>&clientIP=&ts=1581650369353&sig=***</td>
    </tr>
    <tr>
        <td>Output</td>
        <td>
        ```json
        {
            "returnCode": 1,
            "returnMessage": "SUCCESS",
            "data": [
                {
                    "unixtime": "1581650369",
                    "group_id": "",
                    "group_name": "",
                    "server_list": [
                        {
                            "server_id": "1",
                            "server_name": "s1.Hoa Sơn",
                            "status": "1",
                            "ccu": 11
                        },
                        {
                            "server_id": "2",
                            "server_name": "s2.Thái Sơn",
                            "status": "0",
                            "ccu": 11
                        }
                    ]
                }
            ]
        }
        ```
        </td>
    </tr>
</table>

###12. API getTopRanking
* Url: https://$gt_api_domain/app/api/get/getTopRanking
* Protocol: Http GET/POST
* Content-Type: application/x-www-form-urlencoded charset=utf-8
* Input params:

    Tham số|Kiểu dữ liệu|Mô tả
    :-------------|:--------|:----------
    rankType `required`|String|Loại xếp hạng. Ví dụ: lực chiến, thú cưỡi ...	
    clientIP|String|IP browser của người chơi
    serverID|String|ID of server of ranking board
    appID `required`|String|appID cung cấp bởi GT-Framework
    apiKey `required`|String|apiKey cung cấp bởi GT-Framework
    ts `required`|Long|Timestamp hiện tại, đơn vị mili giây
    sig `required`|String|Chữ ký được tạo theo quy tắc [Appendix 1](#appendix-1-create-signature)

* Output params (application/json):

    Tham số|Kiểu dữ liệu|Mô tả
    --------------|---------|-----------
    returnCode|Integer|Mã lỗi <br/>`thành công` = 1 <br/> `thất bại` != 1
    returnMessage|String|Mô tả lỗi
    data|Map <String, RankingEntity\>|Dữ liệu xếp hạng

* RankingEntity:

    Tham số|Kiểu dữ liệu|Mô tả
    --------------|---------|-----------
    rankValue|String|Giá trị xếp hạng
    roleID|String|ID nhân vật
    roleName|StringTên nhân vật
    userID|String|ID người chơi
    serverID|String|ID server của nhân vật này
    loginType|String|Kênh đăng nhập. <br/> Tham chiếu kênh đăng nhập [Appendix 2](#appendix-2-login-type)
    info `optional`|Object (json)|Thông tin thêm của xếp hạng

* Example input/output:
<table>
    <tr>
        <td>API</td>
        <td>getTopRanking</td>
    </tr>
    <tr>
        <td>Method</td>
        <td>POST</td>
    </tr>
    <tr>
        <td>Input</td>
        <td>apiKey=APIKEY123456789&appID=tj&rankingType=power100
        <br>&serverID=1&ts=1581650568516&sig=***</td>
    </tr>
    <tr>
        <td>Output</td>
        <td>
        ```json
        {
            "returnCode": 1,
            "returnMessage": "SUCCESS",
            "data": {
                "1": {
                    "userID": "720578258119450156",
                    "loginType": "",
                    "roleID": "720578258119461861",
                    "roleName": "S8.S8.๖Phi๖Long†€®",
                    "serverID": "1",
                    "rankValue": "171155688",
                    "info": {
                        "accountId": "720578258119450156",
                        "accountName": "dowitung",
                        "createServerId": "8",
                        "gold": "47256",
                        "isForbid": "0",
                        "lastLoginTimes": "1573286212",
                        "power": 171155688
                    }
                }
                ...
            }
        }
        ```
        </td>
    </tr>
</table>

###13. API getPlayerInfo
* Url: https://$gt_api_domain/app/api/get/getPlayerInfo
* Protocol: Http GET/POST
* Content-Type: application/x-www-form-urlencoded charset=utf-8
* Input params:

    Tham số|Kiểu dữ liệu|Mô tả
    --------------|---------|-----------
    clientIP|String|IP browser của người chơi
    userID `required`|String|ID của người chơi
    loginType|String|Kênh đăng nhập. <br/> Tham chiếu kênh đăng nhập [Appendix 2](#appendix-2-login-type)
    serverID `required`|String|ID của server
    roleID `required`|String|ID của nhân vật
    roleName|String|Tên nhân vật
    dataType|String|Mỗi game sẽ có 1 list dataType khác nhau, dataType được cung cấp bởi GT-Framework
    appID `required`|String|appID cung cấp bởi GT-Framework
    apiKey `required`|String|apiKey cung cấp bởi GT-Framework
    ts `required`|Long|Timestamp hiện tại, đơn vị mili giây
    sig `required`|String|Chữ ký được tạo theo quy tắc [Appendix 1](#appendix-1-create-signature)

* Output params (application/json):

    Tham số|Kiểu dữ liệu|Mô tả
    --------------|---------|-----------
    returnCode|Integer|Mã lỗi <br/>`thành công` = 1 <br/> `thất bại` != 1
    returnMessage|String|Mô tả lỗi
    data|Map <String, PlayerInfoEntity\>|Thông tin thêm của nhân vật

* PlayerInfoEntity: 

    Tham số|Kiểu dữ liệu|Mô tả
    --------------|---------|-----------
    roleID|String|ID của nhân vật
    roleName|String|Tên nhân vật
    level|String|Level nhân vật
    userID|String|ID của người chơi
    serverID|String|ID của server của nhân vật này    
    serverName|String|Tên của server của nhân vật này    
    playerInfo|Object (json)|Thông tin thêm của nhân vật, tùy vào dataType là gì    
    info `optional`|Object (json)|Thông tin thêm mở rộng của nhân vật, tùy vào dataType là gì    

* Example input/output:
<table>
    <tr>
        <td>API</td>
        <td>getPlayerInfo</td>
    </tr>
    <tr>
        <td>Method</td>
        <td>POST</td>
    </tr>
    <tr>
        <td>Input</td>
        <td>apiKey=APIKEY123456789&appID=xjm&dataType=getInfoGTL
        <br>&roleID=1046520375&roleName=-Tay-D%C3%A0i-&serverID=20217
        <br>&ts=1581650801669&userID=682103884633694208&sig=***</td>
    </tr>
    <tr>
        <td>Output</td>
        <td>
        ```json
        {
            "returnCode": 1,
            "returnMessage": "SUCCESS",
            "data": {
                "5242885": {
                    "roleID": "5242885",
                    "roleName": "-Tay-D%C3%A0i-",
                    "playerInfo": {
                        "RoleName": "-Tay-D%C3%A0i-",
                        "RetMsg": "ok",
                        "ClothesId": "3251",
                        "Sex": "2",
                        "TireId": "8086",
                        "Job": "19",
                        "ItemId": "12801",
                        "Name": "Server+Review+2",
                        "Result": 0
                    }
                }
            }
        }
        ```
        </td>
    </tr>
</table>

## 3. Example code
```java
import org.apache.commons.codec.binary.Base64;
import org.apache.http.NameValuePair;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.message.BasicNameValuePair;
import org.apache.log4j.Logger;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.math.BigInteger;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

/**
 * @author RotS
 */
public class AppApiTest {
    private static Logger logger = Logger.getLogger(AppApiTest.class);
    public static Comparator<NameValuePair> nameValuePairComp = 
    Comparator.comparing(NameValuePair::getName);
    public static final String API_DOMAIN = "https://api.mto.zing.vn";
    public static void main(String[] args) {
        getServers();
        getServersByUserID();
        getRoles();
        getRolesByRoleID();
        getRolesByUserID();
        getProducts();
        getTrans();
        callbackBilling();
        setCodes();
        addItems();
        getPlayerInfo();
        getCCU();
        getTopRanking();
    }
    
    /// APIs function

    public static void getServers() {
        List<NameValuePair> param = new ArrayList<>();
        param.add(new BasicNameValuePair("serverMode", "getAll"));
        param.add(new BasicNameValuePair("userID", ""));
        param.add(new BasicNameValuePair("loginType", ""));

        createBasicInfo(param
                , "tj" // appID
                , "APIKEY123456789" // apiKey
                , "SECRETKEY123456789"); // secretKey

        String query = URLEncodedUtils.format(param, StandardCharsets.UTF_8);
        String url = API_DOMAIN + "/app/api/get/getServers";
        curl(url, "POST", query);
    }

    public static void getServersByUserID() {
        List<NameValuePair> param = new ArrayList<>();
        param.add(new BasicNameValuePair("userID", "1525197004312092672"));
        param.add(new BasicNameValuePair("loginType", "11"));

        createBasicInfo(param, "cdmt" // appID
                , "APIKEY123456789" // apiKey
                , "SECRETKEY123456789"); // secretKey

        String query = URLEncodedUtils.format(param, StandardCharsets.UTF_8);
        String url = API_DOMAIN + "/app/api/get/getServersByUserID";
        curl(url, "POST", query);
    }

    public static void getRoles() {
        List<NameValuePair> param = new ArrayList<>();
        param.add(new BasicNameValuePair("userID", "1525197004312092672"));
        param.add(new BasicNameValuePair("loginType", "11"));
        param.add(new BasicNameValuePair("serverID", "2"));
        param.add(new BasicNameValuePair("roleID", ""));
        param.add(new BasicNameValuePair("roleName", ""));

        createBasicInfo(param, "cdmt" // appID
                , "APIKEY123456789" // apiKey
                , "SECRETKEY123456789"); // secretKey

        String query = URLEncodedUtils.format(param, StandardCharsets.UTF_8);
        String url = API_DOMAIN + "/app/api/get/getRoles";
        curl(url, "POST", query);
    }

    public static void getRolesByUserID() {
        List<NameValuePair> param = new ArrayList<>();
        param.add(new BasicNameValuePair("userID", "1200695056299851776"));
        param.add(new BasicNameValuePair("loginType", "11"));

        createBasicInfo(param, "hpt" // appID
                , "APIKEY123456789" // apiKey
                , "SECRETKEY123456789"); // secretKey

        String query = URLEncodedUtils.format(param, StandardCharsets.UTF_8);
        String url = API_DOMAIN + "/app/api/get/getRolesByUserID";
        curl(url, "POST", query);
    }

    public static void getRolesByRoleID() {
        List<NameValuePair> param = new ArrayList<>();
        param.add(new BasicNameValuePair("loginType", "6"));
        param.add(new BasicNameValuePair("roleID", "YC3I5"));

        createBasicInfo(param, "autochess" // appID
                , "APIKEY123456789" // apiKey
                , "SECRETKEY123456789"); // secretKey

        String query = URLEncodedUtils.format(param, StandardCharsets.UTF_8);
        String url = API_DOMAIN + "/app/api/get/getRolesByRoleID";
        curl(url, "POST", query);
    }

    public static void getProducts() {
        List<NameValuePair> param = new ArrayList<>();
        param.add(new BasicNameValuePair("clientIP", "49.213.81.56"));
        param.add(new BasicNameValuePair("userID", "1525197004312092672"));
        param.add(new BasicNameValuePair("loginType", "11"));
        param.add(new BasicNameValuePair("serverID", "2"));
        param.add(new BasicNameValuePair("serverName", ""));
        param.add(new BasicNameValuePair("roleID", "1000010"));
        param.add(new BasicNameValuePair("roleName", ""));
        param.add(new BasicNameValuePair("country", "VN"));
        param.add(new BasicNameValuePair("currency", "VND"));

        createBasicInfo(param, "cdmt" // appID
                , "APIKEY123456789" // apiKey
                , "SECRETKEY123456789"); // secretKey

        String query = URLEncodedUtils.format(param, StandardCharsets.UTF_8);
        String url = API_DOMAIN + "/app/api/get/getProducts";
        curl(url, "POST", query);
    }

    public static void getTrans() {
        List<NameValuePair> param = new ArrayList<>();
        param.add(new BasicNameValuePair("clientIP", "49.213.81.56"));
        param.add(new BasicNameValuePair("userID", "1525197004312092672"));
        param.add(new BasicNameValuePair("loginType", "11"));
        param.add(new BasicNameValuePair("serverID", "2"));
        param.add(new BasicNameValuePair("serverName", ""));
        param.add(new BasicNameValuePair("roleID", "1000010"));
        param.add(new BasicNameValuePair("roleName", ""));
        param.add(new BasicNameValuePair("clientID", ""));
        param.add(new BasicNameValuePair("productID", "80vang.cungdau.gs2.vng"));
        param.add(new BasicNameValuePair("orderNumber", "100000000000001"));
        param.add(new BasicNameValuePair("amount", "20000"));
        param.add(new BasicNameValuePair("country", "VN"));
        param.add(new BasicNameValuePair("currency", "VND"));

        createBasicInfo(param, "cdmt" // appID
                , "APIKEY123456789" // apiKey
                , "SECRETKEY123456789"); // secretKey

        String query = URLEncodedUtils.format(param, StandardCharsets.UTF_8);
        String url = API_DOMAIN + "/app/api/get/getTrans";
        curl(url, "POST", query);
    }

    public static void callbackBilling() {
        List<NameValuePair> param = new ArrayList<>();
        param.add(new BasicNameValuePair("gameid", "cdmt"));
        param.add(new BasicNameValuePair("userid", "1525197004312092672"));
        param.add(new BasicNameValuePair("serverid", "2"));
        param.add(new BasicNameValuePair("items", ""));
        param.add(new BasicNameValuePair("amount_origin", ""));
        param.add(new BasicNameValuePair("currency_origin", ""));
        param.add(new BasicNameValuePair("amount_country", ""));
        param.add(new BasicNameValuePair("currency_country", ""));
        param.add(new BasicNameValuePair("apptxnid", "1-1000010-1000394-1574234809"));
        param.add(new BasicNameValuePair("txnid", "100000000000001"));
        param.add(new BasicNameValuePair("addinfo", 
                                    new String(Base64.encodeBase64("3".getBytes()))));
        param.add(new BasicNameValuePair("channel", "1"));
        param.add(new BasicNameValuePair("unum", "1000010"));
        param.add(new BasicNameValuePair("amount", "20000"));
        param.add(new BasicNameValuePair("paymethod", "web"));
        param.add(new BasicNameValuePair("platform", "web"));

        createBasicInfo(param, "cdmt" // appID
                , "APIKEY123456789" // apiKey
                , "SECRETKEY123456789"); // secretKey

        String query = URLEncodedUtils.format(param, StandardCharsets.UTF_8);
        String url = API_DOMAIN + "/app/api/add/callbackBilling";
        curl(url, "POST", query);
    }

    public static void setCodes() {
        List<NameValuePair> param = new ArrayList<>();
        param.add(new BasicNameValuePair("userID", "1525197004312092672"));
        param.add(new BasicNameValuePair("serverID", "2"));
        param.add(new BasicNameValuePair("roleID", "1000010"));
        param.add(new BasicNameValuePair("code", "603add6e"));
        param.add(new BasicNameValuePair("pTransID", "100000000000001"));
        param.add(new BasicNameValuePair("clientIP", "1.1.1.1"));
        param.add(new BasicNameValuePair("addInfo", ""));

        createBasicInfo(param, "cdmt" // appID
                , "APIKEY123456789" // apiKey
                , "SECRETKEY123456789"); // secretKey

        String query = URLEncodedUtils.format(param, StandardCharsets.UTF_8);
        String url = API_DOMAIN + "/app/api/add/setCodes";
        curl(url, "POST", query);
    }

    public static void addItems() {
        List<NameValuePair> param = new ArrayList<>();
        param.add(new BasicNameValuePair("userID", "1525197004312092672"));
        param.add(new BasicNameValuePair("serverID", "2"));
        param.add(new BasicNameValuePair("roleID", "1000010"));
        param.add(new BasicNameValuePair("itemID", "2_10"));
        param.add(new BasicNameValuePair("pTransID", "100000000000001"));
        param.add(new BasicNameValuePair("clientIP", "1.1.1.1"));
        param.add(new BasicNameValuePair("addInfo", ""));

        createBasicInfo(param, "cdmt" // appID
                , "APIKEY123456789" // apiKey
                , "SECRETKEY123456789"); // secretKey

        String query = URLEncodedUtils.format(param, StandardCharsets.UTF_8);
        String url = API_DOMAIN + "/app/api/add/addItems";
        curl(url, "POST", query);
    }

    public static void getPlayerInfo() {
        List<NameValuePair> param = new ArrayList<>();
        param.add(new BasicNameValuePair("userID", "682103884633694208"));
        param.add(new BasicNameValuePair("serverID", "20217"));
        param.add(new BasicNameValuePair("roleID", "1046520375"));
        param.add(new BasicNameValuePair("roleName", "-Tay-Dài-"));
        param.add(new BasicNameValuePair("dataType", "getInfoGTL"));

        createBasicInfo(param, "xjm" // appID
                , "APIKEY123456789" // apiKey
                , "SECRETKEY123456789"); // secretKey

        String query = URLEncodedUtils.format(param, StandardCharsets.UTF_8);
        String url = API_DOMAIN + "/app/api/get/getPlayerInfo";
        curl(url, "POST", query);
    }

    public static void getCCU() {
        List<NameValuePair> param = new ArrayList<>();
        param.add(new BasicNameValuePair("clientIP", ""));

        createBasicInfo(param
                , "tj" // appID
                , "APIKEY123456789" // apiKey
                , "SECRETKEY123456789"); // secretKey

        String query = URLEncodedUtils.format(param, StandardCharsets.UTF_8);
        String url = API_DOMAIN + "/app/api/get/getCCU";
        curl(url, "POST", query);
    }

    public static void getTopRanking() {
        List<NameValuePair> param = new ArrayList<>();
        param.add(new BasicNameValuePair("serverID", ""));
        param.add(new BasicNameValuePair("rankingType", "power100"));

        createBasicInfo(param
                , "tj" // appID
                , "APIKEY123456789" // apiKey
                , "SECRETKEY123456789"); // secretKey

        String query = URLEncodedUtils.format(param, StandardCharsets.UTF_8);
        String url = API_DOMAIN + "/app/api/get/getTopRanking";
        curl(url, "POST", query);
    }

    public static void createBasicInfo(List<NameValuePair> params
    , String appID, String apiKey, String secretKey) {
        try {
            String ts = String.valueOf(System.currentTimeMillis());
            params.add(new BasicNameValuePair("ts", ts));
            params.add(new BasicNameValuePair("appID", appID));
            params.add(new BasicNameValuePair("apiKey", apiKey));
            params.add(new BasicNameValuePair("sig", genSig(secretKey, params)));
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
        }
    }

    public static String genSig(String secretKey, List<NameValuePair> params) {
        try {
            params.sort(nameValuePairComp);

            StringBuilder sb = new StringBuilder();
            sb.append(secretKey);
            params.forEach((param) -> {
                sb.append(param.getValue());
            });
            String sig = md5(sb.toString());
            logger.info("genSig: md5(" + sb.toString() + "): " + sig);
            return sig;
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
        }
        return null;
    }

    public static String md5(String input) {
        try {
            MessageDigest md = MessageDigest.getInstance("MD5");
            byte[] messageDigest = md.digest(input.getBytes());
            BigInteger number = new BigInteger(1, messageDigest);
            String hashtext = number.toString(16);

            while (hashtext.length() < 32) {
                hashtext = "0" + hashtext;
            }
            return hashtext;
        } catch (Exception e) {
            logger.error("md5(" + input + ") exception: " + e.getMessage());
        }
        return "MD5_ERROR";
    }

    public static String curl(String apiDomain, String method, String param) {
        HttpURLConnection conn = null;
        try {
            URL url = new URL(apiDomain);
            conn = (HttpURLConnection) url.openConnection();
            conn.setReadTimeout(15000); // Milliseconds
            conn.setConnectTimeout(15000); // Milliseconds
            conn.setRequestMethod(method);
            conn.setDoOutput(true); // Triggers POST.

            if (method.equals("POST")) {
                try (OutputStream output = conn.getOutputStream()) {
                    output.write(param.getBytes());
                }
            }

            int code = conn.getResponseCode();
            StringBuilder sb;
            try (BufferedReader rd = new BufferedReader(
                    new InputStreamReader(conn.getResponseCode() / 100 == 2
                            ? conn.getInputStream() : conn.getErrorStream()))) {
                sb = new StringBuilder();
                String line;
                while ((line = rd.readLine()) != null) {
                    if (!line.isEmpty()) {
                        sb.append(line);
                    }
                }
            }

            logger.info("Curl: " + url + " - Param: " + param + " - Code: " + code 
            + " - Result: " + sb.toString());
            return sb.toString();

        } catch (Exception e) {
            logger.error("Error at sendPostRequest. " + apiDomain + " - Param: " + param 
            + " - " + e.getMessage(), e);
        } finally {
            if (conn != null) {
                conn.disconnect();
            }
        }
        return null;
    }
}
```

##**Appendix 1. Create signature**

* Chữ ký là 1 chuỗi mã hóa MD5 của các giá trị tham số đã được sắp xếp cùng với một mật mã.

* Mật mã này được tạo bởi VNG hoặc đối tác của VNG và cung cấp cho nhau.

* Tất cả các tham số ngoại trừ Chữ ký sẽ được sử dụng để tạo Chữ ký.

* Các tham số được sắp xếp theo thứ tự ABC.

* Các tham số không được mã hóa.

* Các tham số phân biệt hoa - thường

* Các khoảng trắng đầu và cuối chuỗi sẽ bị loại bỏ.  

* Ví dụ:

    - URL: <span style="font-size:13px;">"https://$domain/$uri?userID=12345&roleID=6789&roleName=test&serverID=1 &gameID=game&itemID=item1&sig=0114778e1349ecffed6c920af210c057 &distSrc=pc&timestamp=1517900546676";</span>
    - Secret key: <span style="font-size:13px;">"pmn4rcocifttkdla8yrwnp0ntnwwze7rpc"</span>
    - Raw: <span style="font-size:13px;">"pmn4rcocifttkdla8yrwnp0ntnwwze7rpc" + "pc" + "game" + "item1" + "6789" + "test" + "1" + "1517900546676" + "12345"</span>
    - Sig: <span style="font-size:13px;">md5("pmn4rcocifttkdla8yrwnp0ntnwwze7rpcpcgameitem16789test1151790054667612345") = "0114778e1349ecffed6c920af210c057"</span>


##**Appendix 2. Login channel**
 
Channel ID| Channel Name
----------|-------------
10|Guest/Email
11|Zing ID (uin)
12|Zalo
13|Google
14|Facebook
15|Zing Me (guid)
16|Line
17|Apple ID

 
##**Appendix 3. Fulfillment guaranty** 

1. Dựa trên chính sách chuyển đổi của VNG và danh sách chuyển đổi của các kênh thanh toán thì số  tiền dùng để mua vật phẩm(tham số amount của API CallbackBilling) sẽ có thể khác số tiền mà người chơi đã thanh toán.
 
2. Dựa trên tỉ giá chuyển đổi cùng với số  tiền nhận được(tham số amount của API CallbackBilling) thì phía Game server quyết định có trao vật phẩm mà người chơi đã chọn mua và các tài nguyên trong game khác hay không.

3. Ví dụ

Vật phẩm mua theo tháng A có giá 100,000 VND, nghĩa là người chơi phải trả 100,000 VND để mua vật phẩm này. Tùy theo giá trị của tham số  *amount* trong [API callbackBilling](#8-api-callbackbilling) như sau:
    
 -  Trường hợp 1: amount = 100,000 
        
    - Game Server giao vật phẩm A bình thường cho người chơi
    
 -  Trường hợp 2: amount = 120,000 
    
    - Game Server giao 1 vật phẩm A vào giao thêm 200 Kim cương in-game(dựa trên tỉ giá 100VND/1 kim cương) cho người chơi
    
 -  Trường hợp 3: amount = 70,000 
    
    - Game Server không giao vật phẩm A và chỉ giao 700 Kim cương in-game(dựa trên tỉ giá 100VND/1 kim cương) cho người chơi