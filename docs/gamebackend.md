# GAME BACKEND APIs

## 1. Mô tả dịch vụ

Là những APIs mà Game Server(do TechOM hoặc Đối tác quản lý) cần cung cấp cho GT Game APIs khi các dịch vụ của VNG có nhu cầu sử dụng dữ liệu


## 2. APIs

<!--
_|_|
-------|-|
Version| 1.0
Release date| 2-2-2020
Mô tả| Là danh sách những APIs cần cung cấp cho GT Game APIs từ Game Backend(do TechOM hoặc Đối tác quản lý) khi các dịch vụ của VNG có nhu cầu sử dụng dữ liệu

##Change log
Version|Mô tả|
-|-|
-->

###Hình 1: Web Shop user flow
![Web Shop user flow](../images/wpflow.jpg)

###**Danh sách APIs**

<!--TOPMENU-->

Tên|Mô tả
----|-----------
[API getServers](#api-getservers) |Liệt kê danh sách tất cả các server của game(trong trường hợp game có nhiều server)
[API getRoles](#api-getroles)|Lấy thông tin của tài khoản/nhân vật theo UserID và ServerID, Xem thêm ở [Appendix 5](#appendix-5-more-info-about-api-getroles)
[API getProducts](#api-getproducts)|Lấy thông tin vật phẩm in-game(gọi chung là vật phẩm) theo 1 số điều kiện nhất định(trong trường hợp game có 1 số quy định khi mua vật phẩm)
[API getTrans](#api-gettrans)|Lấy thông tin transaction ID khi người chơi chọn 1 vật phẩm để mua. Khi người chơi đã thanh toán mua vật phẩm thành công thì transaction ID được gởi về  cho Game-server cùng với thông tin vật phẩm đã mua nhằm mục đích kiểm chứng 
[API callbackBilling](#api-callbackbilling)|Thực hiện trao vật phẩm cho người chơi. 
[API getServersByUserID](#api-getservers-by-userid) |Liệt kê danh sách tất cả các server của game mà người chơi có tạo nhân vật(trong trường hợp game có nhiều server). Giảm thời gian tìm kiếm server.
[API getRolesByRoleID](#api-getroles-by-roleid)|Lấy thông tin của tài khoản/nhân vật theo RoleID, cho phép người chơi nhập roleID in-game để  đăng nhập(đăng nhập nhanh), roleID in-game là duy nhất trên toàn server, roleID in-game dễ  nhớ, dễ copy
[API getRolesByUserID](#api-getroles-by-userid)|Lấy thông tin của tài khoản/nhân vật theo UserID, cho phép người chơi đăng nhập xong không cần chọn server(lấy role đăng nhập gần nhất)
[API getRecommendInfo](#api-getrecommendinfo) |Phục vụ cho tính năng thông báo nạp tích lũy, gói mới, event nạp của game. Đây là những thông tin sẽ được hiển thị cả trong in-game và trên web shop khi người chơi vừa mua vật phẩm thành công(ví dụ: in-game đang có event nạp 500k được nhận thần thú vip, người chơi nạp 100k thì api getRecommendInfo trả về  thông tin: bạn chỉ cần nạp thêm 400 để có thần thú vip...). 
[API addItems](#api-additems)|Trao quà in-game cho người chơi 
[API setCodes](#api-setcodes)|Trao code cho người chơi 
[API getCCU](#api-getccu) |Lấy thông tin CCU của tất cả server của game
[API getTopRanking](#api-gettopranking)|Lấy top nhân vật theo 1 tiêu chí nào đấy
[URL format](#url-format) |Quy định định dạng của liên kết được tạo ra khi mở trang thanh toán từ trong game

<!--ENDTOPMENU-->

###**Tài liệu mô tả APIs**

<div class="pagebreak"></div>
<!--getServers-->

###* **API getServers** 
* Url: https://$game_api_domain/getServers
* Protocol: Http GET/POST
* Content-Type: application/x-www-form-urlencoded charset=utf-8
* Input params:

    Tham số|Kiểu dữ liệu|Mô tả
    :-------------|:--------|:----------
    clientIP|String|IP người chơi
    ts `required`|Long|timestamp thời gian hiện tại, đơn vị mili giây
    sig `required`|String|Chữ ký được tạo theo quy tắc [Appendix 1](#appendix-1-create-signature)

* Output params (application/json):

    Tham số|Kiểu dữ liệu|Mô tả
    --------------|---------|-----------
    returnCode `required`|Integer|Mã lỗi <br/>`Thành công` = 1 <br/> `Thất bại` != 1
    returnMessage|String|Mô tả lỗi
    data `required`|Map <String, ServerEntity\>|Danh sách server, key là ID của server

* ServerEntity:

    Tham số|Kiểu dữ liệu|Mô tả
    --------------|---------|-----------
    serverID `required`|String|ID của server
    serverName `required`|String|Tên của server
    status `required`|Integer|Trạng thái của server <br/> `Hiển thị`=1 <br/> `Ẩn` !=1<br/>Chỉ những server được cho phép mới hiển thị trên trang thanh toán 

* Example input/output:
<table>
    <tr>
        <td>API</td>
        <td>getServers</td>
    </tr>
    <tr>
        <td>Method</td>
        <td>POST</td>
    </tr>
    <tr>
        <td>Input</td>
        <td>
        clientIP=49.213.81.56&ts=1562233802731&sig=***
        </td>
    </tr>
    <tr>
        <td>Output</td>
        <td>
        ```json
        {
            "returnCode": 1,
            "returnMessage": "SUCCESS",
            "data": {
                "110": {
                    "serverName": "s110.Thần Sơn",
                    "serverID": "110",
                    "status": 0,
                    "info": {
                        "merge": "1",
                        "name": "s110.Thần Sơn",
                        "active": false,
                        "id": "110"
                    }
                }
            }
        }
        ```
        </td>
    </tr>
</table>

<!--endgetServers-->
<div class="pagebreak"></div>
<!--getServersByUserID-->

###* **API getServers by UserID** 
* Url: https://$game_api_domain/getServersByUserID
* Protocol: Http GET/POST
* Content-Type: application/x-www-form-urlencoded charset=utf-8
* Input params:

    Tham số|Kiểu dữ liệu|Mô tả
    :-------------|:--------|:----------
    clientIP|String|IP của người chơi
	userID `required`|String|ID của người chơi	
    ts `required`|Long|timestamp thời gian hiện tại, đơn vị mili giây
    sig `required`|String|Chữ ký được tạo theo quy tắc [Appendix 1](#appendix-1-create-signature)

* Output params (application/json):

    Tham số|Kiểu dữ liệu|Mô tả
    --------------|---------|-----------
    returnCode `required`|Integer|Mã lỗi <br/>`Thành công` = 1 <br/> `Thất bại` != 1
    returnMessage|String|Mô tả lỗi
    data `required`|Map <String, ServerEntity\>|Danh sách server, key là ID của server

* ServerEntity:

    Tham số|Kiểu dữ liệu|Mô tả
    --------------|---------|-----------
    serverID `required`|String|ID của server
    serverName `required`|String|Tên của server
    status `required`|Integer|Trạng thái của server <br/> `Hiển thị`=1 <br/> `Ẩn` !=1<br/>Chỉ những server cho phép mới được hiển thị trên trang thanh toán 

* Example input/output:
<table>
    <tr>
        <td>API</td>
        <td>getServersByUserID</td>
    </tr>
    <tr>
        <td>Method</td>
        <td>POST</td>
    </tr>
    <tr>
        <td>Input</td>
        <td>clientIP=49.213.81.56&userID=123&ts=1562233802731&sig=***</td>
    </tr>
    <tr>
        <td>Output</td>
        <td>
        ```json
        {
            "returnCode": 1,
            "returnMessage": "SUCCESS",
            "data": {
                "22": {
                    "serverName": "S20 Từ Ninh Cung",
                    "serverID": "22",
                    "status": 1,
                    "info": {
                        "merge": "1",
                        "name": "s110.Thần Sơn",
                        "active": false,
                        "id": "110"
                    }
                }
            }
        }
        ```
        </td>
    </tr>
</table>

<!--endgetServersByUserID-->
<div class="pagebreak"></div>
<!--getRoles-->

###* **API getRoles**
 
* Url: https://$game_api_domain/getRoles
* Protocol: Http GET/POST
* Content-Type: application/x-www-form-urlencoded charset=utf-8
* Input params:

    Tham số|Kiểu dữ liệu|Mô tả
    --------------|---------|-----------
    clientIP|String|IP của người chơi
    loginChannel|String|Kênh đăng nhập. <br/> Tham chiếu kênh đăng nhập [Appendix 2](#appendix-2-login-channel)
    userID `required`|String|ID của người chơi
    serverID `required`|String|ID của server
    roleID|String|ID của nhân vật
    ts `required`|Long|timestamp thời gian hiện tại, đơn vị mili giây
    sig `required`|String|Chữ ký được tạo theo quy tắc [Appendix 1](#appendix-1-create-signature)

* Output params (application/json):

    Tham số|Kiểu dữ liệu|Mô tả
    --------------|---------|-----------
    returnCode `required`|Integer|Mã lỗi <br/>`Thành công` = 1 <br/> `Thất bại` != 1
    returnMessage|String|Mô tả lỗi
    data `required`|Map <String, RoleEntity\>|Danh sách nhân vật, key là ID của nhân vật

* RoleEntity: 

    Tham số|Kiểu dữ liệu|Mô tả
    --------------|---------|-----------
    roleID `required`|String|ID của nhân vật
    roleName `required`|String|Tên nhân vật
	userID|String|ID của người chơi
    serverID|String|ID của server    
    serverName|String|Tên của server
    info|Object (json)|Những thông tin khác của nhân vật có thể dùng hiển thị trên trang thanh toán <br/> Ví dụ level, xếp hạng, lực chiến ...

* Example input/output:
<table>
    <tr>
        <td>API</td>
        <td>getRoles</td>
    </tr>
    <tr>
        <td>Method</td>
        <td>POST</td>
    </tr>
    <tr>
        <td>Input</td>
        <td>clientIP=49.213.81.56&loginChannel=11&userID=1234<br>&serverID=7059&roleID=&ts=1562233812732&sig=***</td>
    </tr>
    <tr>
        <td>Output</td>
        <td>
        ```json
        {
            "returnCode": 1,
            "returnMessage": "success",
            "data": {
                "70590000002052": {
                    "roleID": "70590000002052",
                    "roleName": "YuiSakura",
                    "userID": "1234",
                    "serverID": "7059",
                    "serverName": "Server 59",
                    "info": {
                        "level": 93,
                        "power": 695020
                    }
                },
                "70600000002053": {
                    "roleID": "70600000002053",
                    "roleName": "YuiSakura 60",
                    "userID": "1234",
                    "serverID": "7060",
                    "serverName": "Server 60",
                    "info": {
                        "level": 12,
                        "power": 9720
                    }
                }
            }
        }
        ```
        </td>
    </tr>
</table>

<!--endgetRoles-->
<div class="pagebreak"></div>
<!--getRolesByRoleID-->

###* **API getRoles by RoleID**
 
* Url: https://$game_api_domain/getRolesByRoleID
* Protocol: Http GET/POST
* Content-Type: application/x-www-form-urlencoded charset=utf-8
* Input params:

    Tham số|Kiểu dữ liệu|Mô tả
    --------------|---------|-----------
    clientIP|String|IP của người chơi    
    roleID `required`|String|ID của nhân vật    
    ts `required`|Long|timestamp thời gian hiện tại, đơn vị mili giây
    sig `required`|String|Chữ ký được tạo theo quy tắc [Appendix 1](#appendix-1-create-signature)

* Output params (application/json):

    Tham số|Kiểu dữ liệu|Mô tả
    --------------|---------|-----------
    returnCode `required`|Integer|Mã lỗi <br/>`Thành công` = 1 <br/> `Thất bại` != 1
    returnMessage|String|Mô tả lỗi
    data `required`|Map <String, RoleEntity\>|Danh sách nhân vật, key là ID của nhân vật

* RoleEntity: 

    Tham số|Kiểu dữ liệu|Mô tả
    --------------|---------|-----------
    roleID `required`|String|ID của nhân vật
    roleName `required`|String|Tên nhân vật
    userID `required`|String|ID của người chơi
    serverID `required`|String|ID của server
    serverName `required`|String|Tên server
    info|Object (json)|Những thông tin khác của nhân vật có thể dùng hiển thị trên trang thanh toán <br/> Ví dụ level, xếp hạng, lực chiến ...

* Example input/output:
<table>
    <tr>
        <td>API</td>
        <td>getRolesByRoleID</td>
    </tr>
    <tr>
        <td>Method</td>
        <td>POST</td>
    </tr>
    <tr>
        <td>Input</td>
        <td>clientIP=49.213.81.56&roleID=12345&ts=1562233812732&sig=***</td>
    </tr>
    <tr>
        <td>Output</td>
        <td>
        ```json
        {
            "returnCode": 1,
            "returnMessage": "SUCCESS",
            "data": {
                "12345": {
                    "roleID": "12345",
                    "roleName": "VoDanh",
                     "serverID": "165",
                    "serverName": "S165-Tiêu Liên Ý",
                    "userID": "1200695056290000000",
                    "info": {}
                }
            }
        }
        ```
        </td>
    </tr>
</table>

<!--endgetRolesByRoleID-->
<div class="pagebreak"></div>
<!--getRolesByUserID-->

###* **API getRoles by UserID**
 
* Url: https://$game_api_domain/getRolesByUserID
* Protocol: Http GET/POST
* Content-Type: application/x-www-form-urlencoded charset=utf-8
* Input params:

    Tham số|Kiểu dữ liệu|Mô tả
    --------------|---------|-----------
    clientIP|String|IP của người chơi    
    userID `required`|String|ID của người chơi    
    ts `required`|Long|timestamp thời gian hiện tại, đơn vị mili giây
    sig `required`|String|Chữ ký được tạo theo quy tắc [Appendix 1](#appendix-1-create-signature)

* Output params (application/json):

    Tham số|Kiểu dữ liệu|Mô tả
    --------------|---------|-----------
    returnCode `required`|Integer|Mã lỗi <br/>`Thành công` = 1 <br/> `Thất bại` != 1
    returnMessage|String|Mô tả lỗi
    data `required`|Map <String, RoleEntity\>|Danh sách nhân vật, key là ID của nhân vật

* RoleEntity: 

    Tham số|Kiểu dữ liệu|Mô tả
    --------------|---------|-----------
    roleID `required`|String|ID của nhân vật
    roleName `required`|String|Tên nhân vật
    userID|String|ID của người chơi
    serverID `required`|String|ID của server
    serverName `required`|String|Tên server
    info|Object (json)|Những thông tin khác của nhân vật có thể dùng hiển thị trên trang thanh toán <br/> Ví dụ level, xếp hạng, lực chiến ...

* Example input/output:
<table>
    <tr>
        <td>API</td>
        <td>getRolesByUserID</td>
    </tr>
    <tr>
        <td>Method</td>
        <td>POST</td>
    </tr>
    <tr>
        <td>Input</td>
        <td>clientIP=49.213.81.56&userID=1234&ts=1562233812732&sig=***</td>
    </tr>
    <tr>
        <td>Output</td>
        <td>
        ```json
        {
            "returnCode": 1,
            "returnMessage": "success",
            "data": {
                "70590000002052": {
                    "roleID": "70590000002052",
                    "roleName": "YuiSakura",
                    "serverID": "7059",
                    "serverName": "Server 59",
                    "userID": "1234",
                    "info": ""
                },
                "70600000002053": {
                    "roleID": "70600000002053",
                    "roleName": "YuiSakura 60",
                    "serverID": "7060",
                    "serverName": "Server 60",
                    "userID": "1234",
                    "info": ""
                }
            }
        }
        ```
        </td>
    </tr>
</table>

<!--endgetRolesByUserID-->
<div class="pagebreak"></div>
<!--getProducts-->

###* **API getProducts** 
* Url: https://$game_api_domain/getProducts
* Protocol: Http GET/POST
* Content-Type: application/x-www-form-urlencoded charset=utf-8
* Input params:

    Tham số|Kiểu dữ liệu|Mô tả
    --------------|---------|-----------
    clientIP|String|IP của người chơi    
    loginChannel|String|Kênh đăng nhập. <br/> Tham chiếu kênh đăng nhập [Appendix 2](#appendix-2-login-channel)
    userID `required`|String|ID của người chơi
    serverID `required`|String|ID của server
    roleID `required`|String|ID của nhân vật
	country|String|Quốc gia. Theo chuẩn ISO 3166-1 alpha-2 code. Ví dụ: VN,TH,IN,… Tham khảo [ISO 3166-1 alpha-2](https://en.wikipedia.org/wiki/ISO_3166-1_alpha-2#Officially_assigned_code_elements).
	currency|String|Tiền tệ. Theo chuẩn ISO 4217. Ví dụ: VND,THB,USD,… Tham khảo [ISO 4217](https://en.wikipedia.org/wiki/ISO_4217#Active_codes).
    ts `required`|Long|timestamp thời gian hiện tại, đơn vị mili giây
    sig `required`|String|Chữ ký được tạo theo quy tắc [Appendix 1](#appendix-1-create-signature)


* Output params (application/json):

    Tham số|Kiểu dữ liệu|Mô tả
    --------------|---------|-----------
    returnCode `required`|Integer|Mã lỗi <br/>`Thành công` = 1 <br/> `Thất bại` != 1
    returnMessage|String|Mô tả lỗi
    data `required`|Map <String, ProductEntity\>|Danh sách vật phẩm, key là ID của vật phẩm

* ProductEntity: 

    Tham số|Kiểu dữ liệu|Mô tả
    --------------|---------|-----------
    productID `required`|String|ID của vật phẩm
    productName|String|Tên vật phẩm
    enable `required`|Boolean|Trạng thái của vật phẩm <br/> `Hiển thị`=1 <br/> `Ẩn` !=1<br/>
    productDescription|String|Mô tả thông tin vật phẩm
    productBonus|String|Thông tin khác
    info|Object(json)|Những thông tin khác cần hiển thị trên trang thanh toán  

* Example input/output:
<table>
    <tr>
        <td>API</td>
        <td>getProducts</td>
    </tr>
    <tr>
        <td>Method</td>
        <td>POST</td>
    </tr>
    <tr>
        <td>Input</td>
        <td>clientIP=49.213.81.56&loginChannel=14&roleID=70500000019908<br>&userID=1234&serverID=7050&country=VN&currency=VND<br>&ts=1562233438476&sig=***</td>
    </tr>
    <tr>
        <td>Output</td>
        <td>
        ```json
        {
            "returnCode": 1,
            "returnMessage": "success",
            "data": {
                "0": {
                    "productID": "0",
                    "enable": true,
                    "productName": "200 Vàng"
                },
                "1": {
                    "productID": "1",
                    "enable": true,
                    "productName": "500 Vàng"
                },
                "2": {
                    "productID": "2",
                    "enable": true,
                    "productName": "1000 Vàng"
                }
            }
        }
        ```
        </td>
    </tr>
</table>

<!--endgetProducts-->
<div class="pagebreak"></div>
<!--getTrans-->

###* **API getTrans**
* Url: https://$game_api_domain/getTrans
* Protocol: Http GET/POST
* Content-Type: application/x-www-form-urlencoded charset=utf-8
* Input params:

    Tham số|Kiểu dữ liệu|Mô tả
    --------------|---------|-----------
    clientIP|String|IP của người chơi
    clientID|String|ID của trang thanh toán(trong trường hợp game có nhiều trang thanh toán)
    loginChannel|String|Kênh đăng nhập. <br/> Tham chiếu kênh đăng nhập [Appendix 2](#appendix-2-login-channel)
    orderNumber `required`|Long|Transaction ID được tạo bởi hệ thống VNG, duy nhất
    userID `required`|String|ID của người chơi    
    serverID `required`|String|ID của server
    roleID `required`|String|ID của nhân vật
    roleName|String|Tên nhân vật        
    productID `required`|String|ID vật phẩm 
    amount `required`|String|Số tiền 
	country|String|Quốc gia. Theo chuẩn ISO 3166-1 alpha-2 code. Ví dụ: VN,TH,IN,…Tham khảo [ISO 3166-1 alpha-2](https://en.wikipedia.org/wiki/ISO_3166-1_alpha-2#Officially_assigned_code_elements).
	currency `required`|String|Tiền tệ. Theo chuẩn ISO 4217. Ví dụ: VND,THB,USD,… Tham khảo [ISO 4217](https://en.wikipedia.org/wiki/ISO_4217#Active_codes).
    ts `required`|Long|timestamp thời gian hiện tại, đơn vị mili giây
    sig `required`|String|Chữ ký được tạo theo quy tắc [Appendix 1](#appendix-1-create-signature)


* Output params (application/json):

    Tham số|Kiểu dữ liệu|Mô tả
    --------------|---------|-----------
    returnCode `required`|Integer|Mã lỗi <br/>`Thành công` = 1 <br/> `Thất bại` != 1
    returnMessage|String|Mô tả lỗi
    data `required`|AppTransEntity Object|Thông tin mã giao dịch

* AppTransEntity:

    Tham số|Kiểu dữ liệu|Mô tả
    --------------|---------|-----------
    appTransID `required`|String|Transaction ID của Game
    appAddInfo|String|Định dạng Json object. Thường chứa thông tin `addinfo` dùng cho [API Billing Callback](#api-callbackbilling) và các thông tin mở rộng khác

* Example input/output:
<table>
    <tr>
        <td>API</td>
        <td>getTrans</td>
    </tr>
    <tr>
        <td>Method</td>
        <td>POST</td>
    </tr>
    <tr>
        <td>Input</td>
        <td>clientIP=49.213.81.56&clientID=<br>&loginChannel=14&orderNumber=1000000000000000000<br>&userID=1234&serverID=7050&roleID=70500000019908<br>&roleName=&productID=0&amount=20000.0<br>&country=VN&currency=VND&ts=1562233438531&sig=***</td>
    </tr>
    <tr>
        <td>Output</td>
        <td>
        ```json
        {
            "returnCode": 1,
            "returnMessage": "success",
            "data": {
                "appTransID": "65b788274a7704675544aa58b8f49fed",
                "appAddInfo": "3110_3_70560000002337_web"
            }
        }
        ```
        </td>
    </tr>
</table>

<!--endgetTrans-->
<div class="pagebreak"></div>
<!--callbackBilling-->

###* **API callbackBilling**
* Url: https://$game_api_domain/callbackBilling
* Protocol: Http GET/POST
* Content-Type: application/x-www-form-urlencoded charset=utf-8
* Input params:

    Tham số|Kiểu dữ liệu|Mô tả
    --------------|---------|-----------
    txnid `required`|String|Transaction ID được tạo bởi hệ thống VNG, duy nhất(chính là orderNumber của api getTrans)
	apptxnid `required`|String|Transaction ID, là kết quả trả về `appTransID` khi gọi api [API getTrans](#api-gettrans). Đây là thông tin dùng để chứng thực giao dịch.
    userid `required`|String|ID của người chơi sẽ nhận vật phẩm trong game
    gameid `required`|String|ID của game (assigned by VNG)
    serverid `required`|String|ID server của người chơi đang chơi
    unum `required`|String|ID của nhân vật trong game, là `roleID` khi gọi api [API getRoles](#api-getroles)
    items `required`|String|ID của vật phẩm hoặc gói vật phẩm mà người chơi mua, là `productID` khi gọi api [API getProducts](#api-getproducts)	
    amount `required`|String|Thông tin này dùng để quyết định vật phẩm có được giao cho người chơi hay không<br/>Tham khảo [Appendix 3](#appendix-3-fulfillment-guaranty)
	country|String|Quốc gia. Theo chuẩn ISO 3166-1 alpha-2 code. Ví dụ: VN,TH,IN,…Tham khảo [ISO 3166-1 alpha-2](https://en.wikipedia.org/wiki/ISO_3166-1_alpha-2#Officially_assigned_code_elements).
	currency `required`|String|Tiền tệ. Theo chuẩn ISO 4217. Ví dụ: VND,THB,USD,… Tham khảo [ISO 4217](https://en.wikipedia.org/wiki/ISO_4217#Active_codes).    channel|String|Kênh thanh toán: zalopay, zing card ...	
    
    Tham số|Kiểu dữ liệu|Mô tả(cont)
    --------------|---------|-----------
    channel `required`|String|ID Kênh thanh toán: `ZaloPay` = 38, `ZingCard` = 1 ...	
    paymethod|String|`webpay` = "web", `android` = "sdk", `ios` = "sdk", `huawei` = "sdk"
    platform|String|`webpay` = "web", `android` = "android", `ios` = "ios", `huawei` = "huawei"
    addinfo|String|Có thể truyền rỗng hoặc null. Với web shop: là kết quả trả về  `appAddInfo` từ  [API getTrans](#api-gettrans). Với IAP (Android/IOS): là thông tin client game truyền cho SDK khi gọi function payment - param "addinfo". 
	extendedInfo|String|Định dạng Jsonstring. Khi game cần truyền thêm thông tin khác thì sử dụng tham số này.
    ts `required`|Long|timestamp thời gian hiện tại, đơn vị mili giây
    sig `required`|String|Chữ ký được tạo theo quy tắc [Appendix 1](#appendix-1-create-signature)

* Output params (application/json):

    Tham số|Kiểu dữ liệu|Mô tả
    --------------|---------|-----------
    returnCode `required`|Integer|Mã lỗi <br/>`Thành công` = 1;<br/> `Trùng giao dịch` = 2 <br/> `Thất bại` < 1
    returnMessage|String|Mô tả lỗi

* Example input/output:
    <table>
        <tr>
            <td>API</td>
            <td>callbackBilling</td>
        </tr>
        <tr>
            <td>Method</td>
            <td>POST</td>
        </tr>
        <tr>
            <td>Input</td>
            <td>
            txnid=1000000000000000000&apptxnid=65b788274a7704675544aa58b8f49fed
            <br>&userid=7050&items=1&gameid=123&serverid=1&unum=70500000019908
            <br>&amount=100&country=VN&currency=VND&channel=zalopay&paymethod=web
            <br>&platform=web&addinfo=3110_3_70560000002337_web&extendedInfo=
            <br>&ts=1562233438531&sig=***
            </td>
        </tr>
        <tr>
            <td>Output</td>
            <td>
            ```json
            {
                "returnCode": 1,
                "returnMessage": "SUCCESS",
                "data": {
                    "returnCode": 1,
                    "returnMessage": "success"
                }
            }
            ```
            </td>
        </tr>
    </table>

<!--endcallbackBilling-->
<!--getRecommendInfo-->

<div class="pagebreak"></div>

###* **API getRecommendInfo**
* Url: https://$game_api_domain/getRecommendInfo
* Protocol: Http GET/POST
* Content-Type: application/x-www-form-urlencoded charset=utf-8
* Input params:

    Tham số|Kiểu dữ liệu|Mô tả
    :-------------|:--------|:----------
    clientIP|String|IP của người chơi
    userID|String|ID của người chơi    
    serverID|String|ID của server
    roleID|String|ID của nhân vật
    recType|String| Phân loại (VD: signal_monthly, signal_accumulation)         
    lang|String|Ngôn ngữ. Theo chuẩn ISO 639-1 codes. Ví dụ: vi,en,… Tham khảo [ISO 639-1 codes](https://en.wikipedia.org/wiki/List_of_ISO_639-1_codes).    
    country|String|Quốc gia. Theo chuẩn ISO 3166-1 alpha-2 codes. Ví dụ: VN,TH,IN,… Tham khảo [ISO 3166-1 alpha-2](https://en.wikipedia.org/wiki/ISO_3166-1_alpha-2#Officially_assigned_code_elements).        
    ts `required`|Long|timestamp thời gian hiện tại, đơn vị mili giây
    sig `required`|String|Chữ ký được tạo theo quy tắc [Appendix 1](#appendix-1-create-signature)

* Output params (application/json):

    Tham số|Kiểu dữ liệu|Mô tả
    --------------|---------|-----------
    returnCode `required`|Integer|Mã lỗi <br/>`Thành công` = 1 <br/> `Thất bại` != 1
    returnMessage|String|Mô tả lỗi
    data `required`|RecommendInfoEntity Object|Dữ liệu cần trả về

* RecommendInfoEntity:

    Tham số|Kiểu dữ liệu|Mô tả
    --------------|---------|-----------
    productID `required`|String|ID sản phẩm
    description|String|Mô tả
    displayText|String|Thông tin(định dạng HTML) cần hiển thị trên trang thanh toán. <br/>Ví dụ: [HOT] Gói A đang mở bán đến ngày ...
    extendedInfo|String|Định dạng Json object. Chứa các thông tin khác

* Example input/output:
<table>
    <tr>
        <td>API</td>
        <td>callbackBilling</td>
    </tr>
    <tr>
        <td>Method</td>
        <td>POST</td>
    </tr>
    <tr>
        <td>Input</td>
        <td>clientIP=49.213.81.56&userID=7050&serverID=1<br>&roleID=70500000019908&recType=signal_monthly<br>&lang=vi&country=VN&ts=1562233438531&sig=***</td>
    </tr>
    <tr>
        <td>Output</td>
        <td>
        ```json
        {
            "returnCode": 1,
            "returnMessage": "success",
            "data": {
                "productID": "1",
                "description": "1",
                "displayText": "1",
                "extendedInfo": {
                    "abc": "1"
                }
            }
        }
        ```
        </td>
    </tr>
</table>

<!--endgetRecommendInfo-->
<div class="pagebreak"></div>
<!--addItems-->

###* **API addItems**
* Url: https://$game_api_domain/addItems
* Protocol: Http GET/POST
* Content-Type: application/x-www-form-urlencoded charset=utf-8
* Input params:

    Tham số|Kiểu dữ liệu|Mô tả
    --------------|---------|-----------    
	userID `required`|String|ID của người chơi
    serverID `required`|String|ID của server
    roleID `required`|String|ID của nhân vật
    itemID `required`|String|ID của item cần gửi cho user
	amount `required`|String|Số lượng item muốn add
    pTransID `required`|String|Transaction ID, là duy nhất
    addInfo|String|Thông tin thêm cần truyền cho Game
    clientIP|String|IP browser của người chơi
    ts `required`|Long|Timestamp hiện tại, đơn vị mili giây
    sig `required`|String|Chữ ký được tạo theo quy tắc [Appendix 1](#appendix-1-create-signature)

* Output params (application/json):

    Tham số|Kiểu dữ liệu|Mô tả
    -----|---------|-----------------
    returnCode `required`|Integer|Mã lỗi <br/>`thành công` = 1;<br/> `trùng pTransID` = -2 <br/> `thất bại` < 1
    returnMessage|String|Mô tả lỗi

* Example input/output:
<table>
    <tr>
        <td>API</td>
        <td>addItems</td>
    </tr>
    <tr>
        <td>Method</td>
        <td>POST</td>
    </tr>
    <tr>
        <td>Input</td>
        <td>roleID=1000010&serverID=2&userID=1525197004312092672&itemID=2
        <br>&amount=10&pTransID=100000000000001&ts=1581649596976&sig=***</td>
    </tr>
    <tr>
        <td>Output</td>
        <td>
        ```json
        {
            "returnCode": 1,
            "returnMessage": "SUCCESS"
        }
        ```
        </td>
    </tr>
</table>

<!--endaddItems-->
<div class="pagebreak"></div>
<!--setCodes-->

###* **API setCodes** 
* Url: https://$game_api_domain/setCodes
* Protocol: Http GET/POST
* Content-Type: application/x-www-form-urlencoded charset=utf-8
* Input params:

    Tham số|Kiểu dữ liệu|Mô tả
    --------------|---------|-----------    
    userID `required`|String|ID của người chơi
    serverID `required`|String|ID của server
    roleID `required`|String|ID của nhân vật
    code `required`|String|Mã code cần nhập
    pTransID `required`|String|Transaction ID, là duy nhất
    addInfo|String|Thông tin thêm cần truyền cho Game
    clientIP|String|IP browser của người chơi
    ts `required`|Long|Timestamp hiện tại, đơn vị mili giây
    sig `required`|String|Chữ ký được tạo theo quy tắc [Appendix 1](#appendix-1-create-signature)

* Output params (application/json):

    Tham số|Kiểu dữ liệu|Mô tả
    --------------|---------|-----------
    returnCode `required`|Integer|Error code <br/>`success` = 1<br/> `fail` != 1
    returnMessage|String|Mô tả lỗi
    
* returnCode:

    returnCode|Mô tả
    ----|---------------------
    1|Nhập code thành công
    -6|Lỗi không xác định
    -50|Tài khoản/ nhân vật không online hoặc không tồn tại
    -44|Code không tồn tại
    -45|code chưa active/code hết hạn
    -46|code đã sử dụng
    -47|đã nhập code cùng loại
    -41|pTransID bị trùng
    
* Example input/output:
<table>
    <tr>
        <td>API</td>
        <td>setCodes</td>
    </tr>
    <tr>
        <td>Method</td>
        <td>POST</td>
    </tr>
    <tr>
        <td>Input</td>
        <td>code=603add&pTransID=100000000000001&roleID=1000010&serverID=2
        <br>&ts=1581649492178&userID=1525197004312092672&sig=***</td>
    </tr>
    <tr>
        <td>Output</td>
        <td>
        ```json
        {
            "returnCode": 1,
            "returnMessage": "SUCCESS"
        }
        ```
        </td>
    </tr>
</table>

<!--endsetCodes-->
<div class="pagebreak"></div>
<!--getCCU-->

###* **API getCCU** 
* Url: https://$game_api_domain/getCCU
* Protocol: Http GET/POST
* Content-Type: application/x-www-form-urlencoded charset=utf-8
* Input params:

    Tham số|Kiểu dữ liệu|Mô tả
    :-------------|:--------|:----------
    clientIP|String|IP of user browser
    ts `required`|Long|Timestamp hiện tại, đơn vị mili giây
    sig `required`|String|Chữ ký được tạo theo quy tắc [Appendix 1](#appendix-1-create-signature)

* Output params (application/json):

    Tham số|Kiểu dữ liệu|Mô tả
    --------------|---------|-----------
    returnCode `required`|Integer|Mã lỗi <br/>`thành công` = 1 <br/> `thất bại` != 1
    returnMessage|String|Mô tả lỗi
    data `required`|List<CcuEntity\>|Danh sách server và CCU tương ứng

* CcuEntity:

    Tham số|Kiểu dữ liệu|Mô tả
    --------------|---------|----------
    serverID `required`|String|ID của server
    serverName|String|Name của server
    status|Integer|Trạng thái của server<br/> `Hoạt động`= 1 <br/> `Không hoạt động`= 1
	ccu `required`|Integer|Số lượng gamer đang chơi game trên 1 server

* Example input/output:
<table>
    <tr>
        <td>API</td>
        <td>getCCU</td>
    </tr>
    <tr>
        <td>Method</td>
        <td>POST</td>
    </tr>
    <tr>
        <td>Input</td>
        <td>clientIP=127.0.0.1&ts=1581650369353&sig=***</td>
    </tr>
    <tr>
        <td>Output</td>
        <td>
        ```json
        {
            "returnCode": 1,
            "returnMessage": "SUCCESS",
            "data": [
                {
                    "serverID": "1",
                    "serverName": "s1.Hoa Sơn",
                    "status": 1,
                    "ccu": 80
                },
                {
                    "serverID": "2",
                    "serverName": "s2.Thái Sơn",
                    "status": 0,
                    "ccu": 11
                }
            ]
        }
        ```
        </td>
    </tr>
</table>

<!--endgetCCU-->
<div class="pagebreak"></div>
<!--getTopRanking-->

###* **API getTopRanking** 
* Url: https://$game_api_domain/getTopRanking
* Protocol: Http GET/POST
* Content-Type: application/x-www-form-urlencoded charset=utf-8
* Input params:

    Tham số|Kiểu dữ liệu|Mô tả
    :-------------|:--------|:----------
    rankType `required`|String|Loại xếp hạng. Ví dụ: lực chiến, thú cưỡi ...	
    clientIP|String|IP browser của người chơi
    serverID|String|ID of server of ranking board
    ts `required`|Long|Timestamp hiện tại, đơn vị mili giây
    sig `required`|String|Chữ ký được tạo theo quy tắc [Appendix 1](#appendix-1-create-signature)

* Output params (application/json):

    Tham số|Kiểu dữ liệu|Mô tả
    --------------|---------|-----------
    returnCode `required`|Integer|Error code <br/>`success` = 1 <br/> `fail` != 1
    returnMessage|String|Error description
    data `required`|Map <String, RankingEntity\>|Dữ liệu xếp hạng

* RankingEntity:

    Tham số|Kiểu dữ liệu|Mô tả
    --------------|---------|-----------
    rankValue `required`|String|Giá trị xếp hạng
    roleID `required`|String|ID nhân vật
    roleName `required`|StringTên nhân vật
    userID|String|ID người chơi
    serverID|String|ID server của nhân vật này
    loginChannel|String|Kênh đăng nhập. <br/> Tham chiếu kênh đăng nhập [Appendix 2](#appendix-2-login-channel)
    info|Object (json)|Thông tin thêm của xếp hạng

* Example input/output:
<table>
    <tr>
        <td>API</td>
        <td>getTopRanking</td>
    </tr>
    <tr>
        <td>Method</td>
        <td>POST</td>
    </tr>
    <tr>
        <td>Input</td>
        <td>rankingType=power100&serverID=1&ts=1581650568516&sig=***</td>
    </tr>
    <tr>
        <td>Output</td>
        <td>
        ```json
        {
            "returnCode": 1,
            "returnMessage": "SUCCESS",
            "data": {
                "1": {
                    "userID": "720578258119450156",
                    "loginType": "",
                    "roleID": "720578258119461861",
                    "roleName": "S8.S8.๖Phi๖Long†€®",
                    "serverID": "1",
                    "rankValue": "171155688",
                    "info": {
                        "accountId": "720578258119450156",
                        "accountName": "dowitung",
                        "createServerId": "8",
                        "gold": "47256",
                        "isForbid": "0",
                        "lastLoginTimes": "1573286212",
                        "power": 171155688
                    }
                }
                ...
            }
        }
        ```
        </td>
    </tr>
</table>

<!--endgetTopRanking-->
<div class="pagebreak"></div>
<!--format-->

###* **URL Format**
* Protocol: Http GET
* Content-Type: application/x-www-form-urlencoded charset=utf-8
* Input params:

    Tham số|Kiểu dữ liệu|Mô tả
    :-------------|:--------|:----------
    lang|String|Ngôn ngữ. Theo chuẩn ISO 639-1 codes. Ví dụ: vi,en,… Tham khảo [ISO 639-1 codes](https://en.wikipedia.org/wiki/List_of_ISO_639-1_codes).    
    country|String|Quốc gia. Theo chuẩn ISO 3166-1 alpha-2 codes. Ví dụ: VN,TH,IN,… Tham khảo [ISO 3166-1 alpha-2](https://en.wikipedia.org/wiki/ISO_3166-1_alpha-2#Officially_assigned_code_elements).    
    currency|String|Tiền tệ. Theo chuẩn ISO 4217. Ví dụ: VND,THB,USD,… Tham khảo [ISO 4217](https://en.wikipedia.org/wiki/ISO_4217#Active_codes).
    userID|String|ID của người chơi    
    roleID|String|ID của nhân vật
    roleName|String|Tên nhân vật
    serverID|String|ID của server
    appID|String|
    productID|String|Là vật phẩm mà người chơi chọn mua. Trang thanh toán chỉ hiển thị duy nhất vật phẩm này. (Nếu giá trị này không truyền hoặc truyền với giá trị là 0 thì trang thanh toán sẽ hiển thị tất cả vật phẩm)
    timestamp|String|Timestamp, thời gian hiện tại(13 digits)
    distSrc|String|Source for this transaction (utm_source)    
    sig `required`|String|Chữ ký. Được tạo bằng cách mã hóa MD5 tất cả các thông tin cần gởi cùng với 1 mật mã.(Cách tạo Sig này khác với các API khác)

<!--endformat-->
<div class="pagebreak"></div>

##**Appendix 1. Create signature**

* Chữ ký là 1 chuỗi mã hóa MD5 của các giá trị tham số đã được sắp xếp cùng với một mật mã.

* Mật mã này được tạo bởi VNG hoặc đối tác của VNG và cung cấp cho nhau.

* Tất cả các tham số ngoại trừ Chữ ký sẽ được sử dụng để tạo Chữ ký.

* Các tham số được sắp xếp theo thứ tự ABC.

* Các tham số không được mã hóa.

* Các tham số phân biệt hoa - thường

* Các khoảng trắng đầu và cuối chuỗi sẽ bị loại bỏ.  

* Ví dụ:

    - URL: <span style="font-size:13px;">"https://$domain/$uri?userID=12345&roleID=6789&roleName=test&serverID=1 &gameID=game&itemID=item1&sig=0114778e1349ecffed6c920af210c057 &distSrc=pc&timestamp=1517900546676";</span>
    - Secret key: <span style="font-size:13px;">"pmn4rcocifttkdla8yrwnp0ntnwwze7rpc"</span>
    - Raw: <span style="font-size:13px;">"pmn4rcocifttkdla8yrwnp0ntnwwze7rpc" + "pc" + "game" + "item1" + "6789" + "test" + "1" + "1517900546676" + "12345"</span>
    - Sig: <span style="font-size:13px;">md5("pmn4rcocifttkdla8yrwnp0ntnwwze7rpcpcgameitem16789test1151790054667612345") = "0114778e1349ecffed6c920af210c057"</span>


##**Appendix 2. Login channel**
 

Channel ID| Channel Name
----------|-------------
10|Guest/Email
11|Zing ID (uin)
12|Zalo
13|Google
14|Facebook
15|Zing Me (guid)
16|Line
17|Apple ID

 
##**Appendix 3. Fulfillment guaranty** 

1. Dựa trên chính sách chuyển đổi của VNG và danh sách chuyển đổi của các kênh thanh toán thì số  tiền dùng để mua vật phẩm(tham số amount của API CallbackBilling) sẽ có thể khác số tiền mà người chơi đã thanh toán.
 
2. Dựa trên tỉ giá chuyển đổi cùng với số  tiền nhận được(tham số amount của API CallbackBilling) thì phía Game server quyết định có trao vật phẩm mà người chơi đã chọn mua và các tài nguyên trong game khác hay không.

3. Ví dụ

Vật phẩm mua theo tháng A có giá 100,000 VND, nghĩa là người chơi phải trả 100,000 VND để mua vật phẩm này. Tùy theo giá trị của tham số  *amount* trong [API callbackBilling](#8-api-callbackbilling) như sau:
    
 -  Trường hợp 1: amount = 100,000 
        
    - Game Server giao vật phẩm A bình thường cho người chơi
    
 -  Trường hợp 2: amount = 120,000 
    
    - Game Server giao 1 vật phẩm A vào giao thêm 200 Kim cương in-game(dựa trên tỉ giá 100VND/1 kim cương) cho người chơi
    
 -  Trường hợp 3: amount = 70,000 
    
    - Game Server không giao vật phẩm A và chỉ giao 700 Kim cương in-game(dựa trên tỉ giá 100VND/1 kim cương) cho người chơi


##**Appendix 4. Duplication Guaranty** 

1. Tất cả giao dịch giữa hệ thống VNG và Game Server phải được xác định bằng `tnxid` và `tnxid` là duy nhất cho một giao dịch
2. Hệ thống Game Server phải có khả năng kiểm tra nếu một giao dịch được thực hiện nhiều lần(chống trao nhầm nhiều lần vật phẩm cho 1 giao dịch). 
3. Nếu có các giao dịch không thành công thì hệ thống VNG sẽ tự động gọi giao hàng lại tới hệ Game Server.  
4. Do 2 hệ thống VNG và Game Server là 2 hệ thống độc lập và bất đồng bộ nên có thể sảy ra trường hợp người chơi có thể mua nhiều hơn 1 lần với vật phẩm đặc biệt(luật chỉ cho phép mua 1 lần duy nhất). Vì vậy phía Game Server nên có cơ chế đền bù cho người chơi trong trường hợp này.

**Ví dụ: Gói thẻ tháng cho phép người chơi chỉ mua 1 lần duy nhất trong tháng.**

 -  Lần 1 người chơi mua gói thẻ tháng bằng hình thức thanh toán ngân hàng thì thanh toán bị chậm từ phía ngân hàng và người chơi chưa nhận được vật phẩm
 -  Lần 2 người chơi tiếp tục mua lại gói thẻ tháng bằng Zalopay và nhận hàng thành công
 -  Sau đó phía ngân hàng thông báo thanh toán thành công cho lần 1. Hệ thống VNG thực hiện giao hàng và quá trình giao hàng bị thất bại vì vi phạm quy định "Gói thẻ tháng người chơi chỉ mua 1 lần duy nhất trong tháng"
 -  Vì vậy phía Game Server và phía vận hành nên có cơ chế đền bù cho người chơi trong trường hợp này.
 -  Đề xuất các phương án đền bù:
    -  Phía Game Server tự động trao người chơi 1 số tiền in-game tương ứng
    -  Phía Game Server tự động trao người chơi 1 vật phẩm tương ứng
    -  Phía vận hành game đền bù thủ công cho người chơi thông qua GM-Tool
    
##**Appendix 5. More info about API getRoles** 

- Nhóm APIs getRoles có 3 chức năng chính, nêu ở mục [Danh sách APIs](#danh-sach-apis) có thể chia làm 3 APIs riêng biệt getRoles(1), getRolesByUserID(2), getRolesByRoleID(3) hoặc gộp chung vào api getRoles đều được:
 
- APIs bắt buộc phải có 1 trong 2 chức năng 1 hoặc 2 để Web Shop Và hệ thống của các team khác có thể hoạt động bình thường. Nếu đã có chức năng 2 thì API getServersByUserID sẽ không được sử dụng.

- Khi gộp 3 APIs lại với nhau, input bắt buộc phải có roleID hoặc userID, đối tác cần dựa vào input truyền vào để xử lý đúng mục đích:
 
    - Nếu có truyền roleID => Xử lý như getRolesByRoleID
    
    - Nếu không truyền roleID, không truyền serverID => Xử lý như getRolesByUserID
    
    - Nếu không truyền roleID, có truyền serverID => Xử lý như getRoles